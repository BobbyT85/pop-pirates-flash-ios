package  
{
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.net.SharedObject;
import flash.text.TextField;
import com.milkmangames.nativeextensions.ios.GameCenter;
import com.milkmangames.nativeextensions.ios.events.GameCenterErrorEvent;
import com.milkmangames.nativeextensions.ios.events.GameCenterEvent;

/** GameCenterExample App */
public class GameCenterExample extends Sprite
{
	//
	// Definitions
	//
	
	/** Leaderboard ID
	 * 
	 * This needs to exactly match a leaderboard you created in iTunes Connect!
	 * */
	private static const LEADERBOARD_ID:String="airleaderboard";
	
	/** Achievement ID 
	 * 
	 * Must exactly match an achievement you created in iTunes Connect!
	 * 
	 * */
	private static const ACHIEVEMENT_ID:String="airachievement01";

	//
	// Instance Variables
	//
	
	/** Status */
	private var txtStatus:TextField;
	
	/** Buttons */
	private var buttonContainer:Sprite;
	
	/** Score */
	private var score:int;
	
	/** Score TextField */
	private var txtScore:TextField;
	
	/** Shared Object */
	private var sharedObject:SharedObject;
	
	//
	// Public Methods
	//
	
	/** Create New GameCenterExample */
	public function GameCenterExample() 
	{		
		// load the stored local score, if there is one.
		loadScore();
		
		createUI();
		init();
	}
		
	/** Init */
	public function init():void
	{
		// check if gameCenter is supported.  note that this just determines platform support- iOS- and NOT whether
		// the user's os version has gamecenter installed!
		if (!GameCenter.isSupported())
		{
			log("GameCenter is not supported on this platform.");
			removeChild(buttonContainer);
			return;
		}
		
		var gameCenter:GameCenter;
		log("initializing GameCenter...");		
		try
		{			
			gameCenter=GameCenter.create(stage);
		}
		catch (e:Error)
		{
			log("ERROR:"+e.message);
			return;
		}
		log("GameCenter Initialized.");
		
		log("Checking os level support...");
		
		
		// GameCenter doesn't work on iOS versions < 4.1, so always check this first!
		if (!gameCenter.isGameCenterAvailable())
		{
			log("this ios version doesn't have gameCenter.");
			removeChild(buttonContainer);
			return;
		} 
		
		log("Game Center is ready.");
		
		// ios5.0+ can show achievement notifications in the native UI.  
		if (GameCenter.gameCenter.areBannersAvailable())
		{
			GameCenter.gameCenter.showAchievementBanners(true);
		}
		
		// this is the complete suite of supported events.  you may not need to listen to all of them for your app,
		// however you should always listen for at least the 'failed' error events to avoid your app throwing errors.
		gameCenter.addEventListener(GameCenterEvent.AUTH_SUCCEEDED,onAuthSucceeded);
		gameCenter.addEventListener(GameCenterErrorEvent.AUTH_FAILED,onAuthFailed);
		gameCenter.addEventListener(GameCenterEvent.ACHIEVEMENTS_VIEW_OPENED,onViewOpened);
		gameCenter.addEventListener(GameCenterEvent.ACHIEVEMENTS_VIEW_CLOSED,onViewClosed);
		gameCenter.addEventListener(GameCenterEvent.LEADERBOARD_VIEW_OPENED,onViewOpened);
		gameCenter.addEventListener(GameCenterEvent.LEADERBOARD_VIEW_CLOSED,onViewClosed);
		gameCenter.addEventListener(GameCenterEvent.ACHIEVEMENT_REPORT_SUCCEEDED,onAchievementReported);
		gameCenter.addEventListener(GameCenterEvent.ACHIEVEMENT_RESET_SUCCEEDED,onAchievementReset);
		gameCenter.addEventListener(GameCenterEvent.SCORE_REPORT_SUCCEEDED,onScoreReported);
		gameCenter.addEventListener(GameCenterErrorEvent.SCORE_REPORT_FAILED,onScoreFailed);
		gameCenter.addEventListener(GameCenterErrorEvent.ACHIEVEMENT_REPORT_FAILED,onAchievementFailed);
		gameCenter.addEventListener(GameCenterErrorEvent.ACHIEVEMENT_RESET_FAILED,onResetFailed);		
	}
	
	/** Authenticate Local User */
	public function authenticateUser():void
	{
		log("try auth user.");
		GameCenter.gameCenter.authenticateLocalUser();
		log("waiting for auth...");
	}
	
	/** Increase local score */
	public function increaseScore():void
	{
		this.score++;
		this.txtScore.text="Score: "+this.score;
		saveScore();
	}
	
	/** Report score */
	public function reportScore():void
	{
		// we make sure you're logged in before bothering to report the score.
		// later iOS versions may take care of waiting/resubmitting for you, but earlier ones won't.
		if (!checkAuthentication()) return;
		GameCenter.gameCenter.reportScoreForCategory(this.score,LEADERBOARD_ID);
	}
	
	/** Report Achievement */
	public function reportAchievement():void
	{
		if (!checkAuthentication()) return;
		
		// the '1.0' is a float (Number) value from 0.0-100.0 the percent completion of the achievement.
		GameCenter.gameCenter.reportAchievement(ACHIEVEMENT_ID,100.0);
	}
	
	/** Show Leaderboards */
	public function showLeaderboard():void
	{
		if (!checkAuthentication()) return;
		GameCenter.gameCenter.showLeaderboardForCategory(LEADERBOARD_ID);
	}
	
	/** Show Achievements */
	public function showAchievements():void
	{
		if (!checkAuthentication()) return;
		
		log("showing achievements...");
		try
		{
			GameCenter.gameCenter.showAchievements();
		}
		catch (e:Error)
		{
			log("ERR showachievements:"+e.message+"/"+e.name+"/"+e.errorID);
		}
		
	}
	
	/** Reset Achievements */
	public function resetAchievements():void
	{
		if (!checkAuthentication()) return;
		
		GameCenter.gameCenter.resetAchievements();
	}

	//
	// Events
	//
	
	private function onAuthSucceeded(e:GameCenterEvent):void
	{
		log("Auth succeeded!");
		showFullUI();
		log("auth player:"+GameCenter.gameCenter.getPlayerAlias()+"="+GameCenter.gameCenter.getPlayerID()+",underage?"+GameCenter.gameCenter.isPlayerUnderage());
	}
	
	private function onAuthFailed(e:GameCenterErrorEvent):void
	{
		log("Auth failed:"+e.message);
		showAuthUI();
	}
	
	private function onViewOpened(e:GameCenterEvent):void
	{
		log("gamecenter view opened.");
	}
	private function onViewClosed(e:GameCenterEvent):void
	{
		log("gamecenter view closed.");
	}
	private function onAchievementReported(e:GameCenterEvent):void
	{
		log("achievement report success:"+e.achievementID);
	}
	private function onAchievementFailed(e:GameCenterErrorEvent):void
	{
		log("achievement report failed:msg="+e.message+",cd="+e.errorID+",ach="+e.achievementID);
	}
	private function onScoreReported(e:GameCenterEvent):void
	{
		log("score report success:"+e.score+"/"+e.category);
	}
	private function onScoreFailed(e:GameCenterErrorEvent):void
	{
		log("score report failed:msg="+e.message+",cd="+e.errorID+",scr="+e.score+",cat="+e.category);
	}
	private function onAchievementReset(e:GameCenterEvent):void
	{
		log("achievements reset.");
		this.score=0;
		saveScore();
		this.txtScore.text="Score: "+score;
	}
	private function onResetFailed(e:GameCenterErrorEvent):void
	{
		log("failed to reset:"+e.message);
	}	
	
	//
	// Impelementation
	//
	

	/** Log */
	private function log(msg:String):void
	{
		trace("[GameCenterExample] "+msg);
		txtStatus.text=msg;
	}
	
	/** Load Score */
	private function loadScore():void
	{
		sharedObject=SharedObject.getLocal("airgc");
		this.score=sharedObject.data["score"]||0;
	}
	
	/** Save Score */
	private function saveScore():void
	{
		sharedObject.data["score"]=this.score;
		sharedObject.flush();
	}
	
	/** Check Authentication */
	private function checkAuthentication():Boolean
	{
		if (!GameCenter.gameCenter.isUserAuthenticated())
		{
			log("not logged in!");
			return false;
		}
		return true;
	}
	
	/** Create UI */
	public function createUI():void
	{
		txtStatus=new TextField();
		txtStatus.defaultTextFormat=new flash.text.TextFormat("Arial",15);
		txtStatus.width=stage.stageWidth;
		txtStatus.multiline=true;
		txtStatus.wordWrap=true;
		txtStatus.text="Ready";
		addChild(txtStatus);
		
		txtScore=new TextField();
		txtScore.defaultTextFormat=new flash.text.TextFormat("Arial",30);
		txtScore.width=stage.stageWidth;
		txtScore.height=30;
		txtScore.multiline=false;
		txtScore.text="Score: "+score;
		txtScore.y=stage.stageHeight-txtScore.height;
		addChild(txtScore);
		
		showAuthUI();
	}
	
	/** Show an 'authorize user' button only (not logged in) */
	public function showAuthUI():void
	{
		if (buttonContainer)
		{
			removeChild(buttonContainer);
			buttonContainer=null;
		}
		
		buttonContainer=new Sprite();
		buttonContainer.y=txtStatus.height;
		addChild(buttonContainer);
		
		var uiRect:Rectangle=new Rectangle(0,0,stage.stageWidth,stage.stageHeight);
		var layout:ButtonLayout=new ButtonLayout(uiRect,14);
		layout.addButton(new SimpleButton(new Command("Authenticate User",authenticateUser)));
		layout.attach(buttonContainer);
		layout.layout();
	}
	
	/** Show full UI Options (is logged in) */
	public function showFullUI():void
	{
		if (buttonContainer)
		{
			removeChild(buttonContainer);
			buttonContainer=null;
		}
		
		buttonContainer=new Sprite();
		buttonContainer.y=txtStatus.height;
		addChild(buttonContainer);
		
		var uiRect:Rectangle=new Rectangle(0,0,stage.stageWidth,stage.stageHeight);
		var layout:ButtonLayout=new ButtonLayout(uiRect,14);
		layout.addButton(new SimpleButton(new Command("Increase score",increaseScore)));
		layout.addButton(new SimpleButton(new Command("Report Score",reportScore)));
		layout.addButton(new SimpleButton(new Command("Report Achievement",reportAchievement)));
		layout.addButton(new SimpleButton(new Command("Show Leaderboard",showLeaderboard)));
		layout.addButton(new SimpleButton(new Command("Show Achievements",showAchievements)));
		layout.addButton(new SimpleButton(new Command("Reset Achievements",resetAchievements)));

		layout.attach(buttonContainer);
		layout.layout();	
	}
	
	
}
}

//
// Code Below is generic code for building UI
//


import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

/** Simple Button */
class SimpleButton extends Sprite
{
	//
	// Instance Variables
	//
	
	/** Command */
	private var cmd:Command;
	
	/** Width */
	private var _width:Number;
	
	/** Label */
	private var txtLabel:TextField;
	
	//
	// Public Methods
	//
	
	/** Create New SimpleButton */
	public function SimpleButton(cmd:Command)
	{
		super();
		this.cmd=cmd;
		
		mouseChildren=false;
		mouseEnabled=buttonMode=useHandCursor=true;
		
		txtLabel=new TextField();
		txtLabel.defaultTextFormat=new TextFormat("Arial",42,0xFFFFFF);
		txtLabel.mouseEnabled=txtLabel.mouseEnabled=txtLabel.selectable=false;
		txtLabel.text=cmd.getLabel();
		txtLabel.autoSize=TextFieldAutoSize.LEFT;
		
		redraw();
		
		addEventListener(MouseEvent.CLICK,onSelect);
	}
	
	/** Set Width */
	override public function set width(val:Number):void
	{
		this._width=val;
		redraw();
	}

	
	/** Dispose */
	public function dispose():void
	{
		removeEventListener(MouseEvent.CLICK,onSelect);
	}
	
	//
	// Events
	//
	
	/** On Press */
	private function onSelect(e:MouseEvent):void
	{
		this.cmd.execute();
	}
	
	//
	// Implementation
	//
	
	/** Redraw */
	private function redraw():void
	{		
		txtLabel.text=cmd.getLabel();
		_width=_width||txtLabel.width*1.1;
		
		graphics.clear();
		graphics.beginFill(0x444444);
		graphics.lineStyle(2,0);
		graphics.drawRoundRect(0,0,_width,txtLabel.height*1.1,txtLabel.height*.4);
		graphics.endFill();
		
		txtLabel.x=_width/2-(txtLabel.width/2);
		txtLabel.y=txtLabel.height*.05;
		addChild(txtLabel);
	}
}

/** Button Layout */
class ButtonLayout
{
	private var buttons:Array;
	private var rect:Rectangle;
	private var padding:Number;
	private var parent:DisplayObjectContainer;
	
	public function ButtonLayout(rect:Rectangle,padding:Number)
	{
		this.rect=rect;
		this.padding=padding;
		this.buttons=new Array();
	}
	
	public function addButton(btn:SimpleButton):uint
	{
		return buttons.push(btn);
	}
	
	public function attach(parent:DisplayObjectContainer):void
	{
		this.parent=parent;
		for each(var btn:SimpleButton in this.buttons)
		{
			parent.addChild(btn);
		}
	}
	
	public function layout():void
	{
		var btnX:Number=rect.x+padding;
		var btnY:Number=rect.y;
		for each( var btn:SimpleButton in this.buttons)
		{
			btn.width=rect.width-(padding*2);
			btnY+=this.padding;
			btn.x=btnX;
			btn.y=btnY;
			btnY+=btn.height;
		}
	}
}

/** Inline Command */
class Command
{
	/** Callback Method */
	private var fnCallback:Function;
	
	/** Label */
	private var label:String;
	
	//
	// Public Methods
	//
	
	/** Create New Command */
	public function Command(label:String,fnCallback:Function)
	{
		this.fnCallback=fnCallback;
		this.label=label;
	}
	
	//
	// Command Implementation
	//
	
	/** Get Label */
	public function getLabel():String
	{
		return label;
	}
	
	/** Execute */
	public function execute():void
	{
		fnCallback();
	}
}