﻿/**
* FORMULA 1 
* Swear filter class
* 
* @description 
* Control navigation, copy
* and events
* 
* @clip
* 
*************************************
* @author Xavier Monvoisin
*************************************/
import mx.utils.Delegate;

//
class com.iainlobb.SwearFilter{
	
	private var data:Object={};
	private var swearwords_array = ["ass", "semen", "shyt", "shirtlifter", "screw", "pube", "sado", "redwings", "ragweek", "quim", "pubic", "prick", "poof", "poontang", "piss", "phuck", "phuk", "piss", "penis", "pecker", "nympho", "pederast", "minge", "fook", "niga", "nigga", "mastur", "muff", "jiz", "marijuana", "mariuana", "lesb", "mofo", "ladyboy", "lez", "knacker", "jism", "hardon", "hairpie", "knob", "giz", "grope", "bang", "genital", "gook", "gang", "fuk", "ganja", "arse", "penis", "klit", "fisting", "flap", "flid", "fellatio", "fanny", "Feck", "felch", "fuck", "cunt", "cunny", "bitch", "whore", "drug", "queer", "gay", "paedo", "pedo", "anal", "anus", "sex", "lingus", "piss", "rape", "nigger", "niger", "coon", "koon", "chink", "fag", "bastard", "retard", "jackoff", "jerk", "wank", "tit", "curtain", "bellend", "bestial", "biatch", "scrotum", "scrote", "shit", "blow", "bollock", "bollox", "bondage", "boner", "rapist", "bugger", "bum", "butt", "cameltoes", "clit","cock","shag", "crap", "cum", "dago", "darkie", "darky", "dick", "dildo", "dong", "dik", "fuc", "slope", "uncletom", "taint", "sphinct", "slant", "sambo", "raghead", "towelhead", "rugmunch", "rimjob", "rimming", "kum", "phelch", "jap", "aids", "fudgepacker", "beefinjection", "foreskin", "forskin", "crack", "fart", "hiv", "babybatter", "skat", "homo", "hore", "perv", "oral", "onanist", "snatch", "bich", "bdsm", "porn", "poontang", "Arsch", "drug", "urin", "poo", "Testicles", "schlong", "rectal", "bonk", "whore", "felch", "licker", "Mongoloid", "Mongaloid", "boob", "sex", "erect", "Osama", "franco", "STALIN", "MUSSOLINI", "fashist", "facist", "GESTAPO", "GULAG", "Lesba", "Whitepower", "SiegHeil", "Saddam", "Sadam", "polac", "polak", "Bin Laden", "pussy", "nazi", "Neger", "kaffer", "kaffir", "kafer", "kafir", "Fuehrer", "gobbels", "Hitler", "Himmler", "Himler", "Goering", "Fuhrer", "ejacu", "slut", "smeg", "sodo", "spank", "spas", "spaz", "sperm", "splif", "spunk", "stiff", "teabag", "turd", "twat", "uphillgardner", "vagin", "vibrator", "vulv", "wank", "whackoff", "wog", "wop", "zoo", "christ", "hash", "penis", "phuk", "coke", "phux", "bondage", "breast", "sausagejockey", "punani", "phuq", "beaver", "brothel", "carnal", "Paki", "Packi", "packy", "bordello", "bender", "merd", "scheiz", "cacca", "lsd", "gspot", "flap", "jew", "viol", "phile", "wog", "wop", "yid", "molest", "stoned", "toss", "fick", "Orgas", "fcuk", "suck", "vibra", "vibro"];
	//
/******************************************************************
* PUBLIC METHODS
*******************************************************************/
	public function SwearFilter(){
		super();
	}
	
	public function check(mytxt:String):Boolean{
		var mytxt = stripSpaces(mytxt);
		var myreturn = true;
		if (mytxt.length > 0) {
			if (!inArray(swearwords_array, mytxt)) {
				if (!wordsWithinArray(swearwords_array, mytxt)) {
				} else {
					myreturn = false;
				}
			} else {
				myreturn = false;
			}
		} else {
			myreturn = false;
		}
		trace("acceptable? "+myreturn);
		return myreturn;
	}
	
/******************************************************************
* PRIVATE METHODS
*******************************************************************/
	
	private function stripSpaces(the_str:String) {
		var my_array = the_str.split("");
		var new_array = [];
		for (var i = 0; i < my_array.length; i++) {
			if (my_array[i] != " ") {
				new_array.push(my_array[i]);
			}
		}
		var new_str = new_array.join("");
		return new_str;
	}
	
	
	private function inArray(theArray:Array, sentWord:String) {
		var found = false;
		var l = theArray.length;
		for (var i = 0; i < l; i++) {
			var listWord = theArray[i].toLowerCase();
			var sentWordLowercase = sentWord.toLowerCase();
			if (listWord == sentWordLowercase) {
				found = true;
			}
		}
		if (found) {
			return true;
		} else {
			return false;
		}
	}


	private function containsWord(theWord:String, delimiter:String) {
		if (theWord.split(delimiter).length > 1) {
			return true;
		} else {
			return false;
		}
	}

	private function wordsWithinArray(theArray:Array, theWord:String) {
		var found = false;
		var l = theArray.length;
		var theWord = theWord.toLowerCase();
		for (var i = 0; i < l; i++) {
			var arrayWord = theArray[i].toLowerCase();
			if (containsWord(theWord, arrayWord)) {
				found = true;
			}
		}
		if (found) {
			return true;
		} else {
			return false;
		}
	}	
		
	

/******************************************************************
* HANDLERS
*******************************************************************/
	
		
}
	 