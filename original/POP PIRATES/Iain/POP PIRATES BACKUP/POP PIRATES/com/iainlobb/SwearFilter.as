class com.iainlobb.SwearFilter
{
    function SwearFilter()
    {
        super();
    } // End of the function
    function check(mytxt)
    {
        mytxt = this.stripSpaces(mytxt);
        var _loc2 = true;
        if (mytxt.length > 0)
        {
            if (!this.inArray(swearwords_array, mytxt))
            {
                if (!this.wordsWithinArray(swearwords_array, mytxt))
                {
                }
                else
                {
                    _loc2 = false;
                } // end else if
            }
            else
            {
                _loc2 = false;
            } // end else if
        }
        else
        {
            _loc2 = false;
        } // end else if
        return (_loc2);
    } // End of the function
    function stripSpaces(the_str)
    {
        var _loc2 = the_str.split("");
        var _loc3 = [];
        for (var _loc1 = 0; _loc1 < _loc2.length; ++_loc1)
        {
            if (_loc2[_loc1] != " ")
            {
                _loc3.push(_loc2[_loc1]);
            } // end if
        } // end of for
        var _loc4 = _loc3.join("");
        return (_loc4);
    } // End of the function
    function inArray(theArray, sentWord)
    {
        var _loc4 = false;
        var _loc5 = theArray.length;
        for (var _loc1 = 0; _loc1 < _loc5; ++_loc1)
        {
            var _loc3 = theArray[_loc1].toLowerCase();
            var _loc2 = sentWord.toLowerCase();
            if (_loc3 == _loc2)
            {
                _loc4 = true;
            } // end if
        } // end of for
        if (_loc4)
        {
            return (true);
        }
        else
        {
            return (false);
        } // end else if
    } // End of the function
    function containsWord(theWord, delimiter)
    {
        if (theWord.split(delimiter).length > 1)
        {
            return (true);
        }
        else
        {
            return (false);
        } // end else if
    } // End of the function
    function wordsWithinArray(theArray, theWord)
    {
        var _loc4 = false;
        var _loc5 = theArray.length;
        theWord = theWord.toLowerCase();
        for (var _loc2 = 0; _loc2 < _loc5; ++_loc2)
        {
            var _loc3 = theArray[_loc2].toLowerCase();
            if (this.containsWord(theWord, _loc3))
            {
                _loc4 = true;
            } // end if
        } // end of for
        if (_loc4)
        {
            return (true);
        }
        else
        {
            return (false);
        } // end else if
    } // End of the function
    var data = {};
    var swearwords_array = ["ass", "semen", "shyt", "shirtlifter", "screw", "pube", "sado", "redwings", "ragweek", "quim", "pubic", "prick", "poof", "poontang", "piss", "phuck", "phuk", "piss", "penis", "pecker", "nympho", "pederast", "minge", "fook", "niga", "nigga", "mastur", "muff", "jiz", "marijuana", "mariuana", "lesb", "mofo", "ladyboy", "lez", "knacker", "jism", "hardon", "hairpie", "knob", "giz", "grope", "bang", "genital", "gook", "gang", "fuk", "ganja", "arse", "penis", "klit", "fisting", "flap", "flid", "fellatio", "fanny", "Feck", "felch", "fuck", "cunt", "cunny", "bitch", "whore", "drug", "queer", "gay", "paedo", "pedo", "anal", "anus", "sex", "lingus", "piss", "rape", "nigger", "niger", "coon", "koon", "chink", "fag", "bastard", "retard", "jackoff", "jerk", "wank", "tit", "curtain", "bellend", "bestial", "biatch", "scrotum", "scrote", "shit", "blow", "bollock", "bollox", "bondage", "boner", "rapist", "bugger", "bum", "butt", "cameltoes", "clit", "cock", "shag", "crap", "cum", "dago", "darkie", "darky", "dick", "dildo", "dong", "dik", "fuc", "slope", "uncletom", "taint", "sphinct", "slant", "sambo", "raghead", "towelhead", "rugmunch", "rimjob", "rimming", "kum", "phelch", "jap", "aids", "fudgepacker", "beefinjection", "foreskin", "forskin", "crack", "fart", "hiv", "babybatter", "skat", "homo", "hore", "perv", "oral", "onanist", "snatch", "bich", "bdsm", "porn", "poontang", "Arsch", "drug", "urin", "poo", "Testicles", "schlong", "rectal", "bonk", "whore", "felch", "licker", "Mongoloid", "Mongaloid", "boob", "sex", "erect", "Osama", "franco", "STALIN", "MUSSOLINI", "fashist", "facist", "GESTAPO", "GULAG", "Lesba", "Whitepower", "SiegHeil", "Saddam", "Sadam", "polac", "polak", "Bin Laden", "pussy", "nazi", "Neger", "kaffer", "kaffir", "kafer", "kafir", "Fuehrer", "gobbels", "Hitler", "Himmler", "Himler", "Goering", "Fuhrer", "ejacu", "slut", "smeg", "sodo", "spank", "spas", "spaz", "sperm", "splif", "spunk", "stiff", "teabag", "turd", "twat", "uphillgardner", "vagin", "vibrator", "vulv", "wank", "whackoff", "wog", "wop", "zoo", "christ", "hash", "penis", "phuk", "coke", "phux", "bondage", "breast", "sausagejockey", "punani", "phuq", "beaver", "brothel", "carnal", "Paki", "Packi", "packy", "bordello", "bender", "merd", "scheiz", "cacca", "lsd", "gspot", "flap", "jew", "viol", "phile", "wog", "wop", "yid", "molest", "stoned", "toss", "fick", "Orgas", "fcuk", "suck", "vibra", "vibro"];
} // End of Class
