﻿class com.iainlobb.ScrollingClip extends MovieClip {
	var scroll_mc:MovieClip;
	var scroll_height:Number;
	var scroll_base:Number;
	var viewable_height:Number;
	var scroll_txt:TextField;
	var title_txt:TextField;
	public var header_height:Number;
	public var image_mc:MovieClip;
	function ScrollingClip() {
		trace("scrolling clip created");
		//scroll_mc = _parent.scroll_mc;
		scroll_height = 0;
		//viewable_height = 589;
		header_height = 0;
		//scroll_base = 30;
	}
	function init(ini_scroll_mc, ini_viewable_height, ini_scroll_base, ini_scroll_height) {
		scroll_mc = ini_scroll_mc;
		viewable_height = ini_viewable_height;
		scroll_base = ini_scroll_base;
		scroll_height = ini_scroll_height-viewable_height;
		scroll_mc.resetSlider();
		if (scroll_height<0) {
			scroll_height = 0;
			scroll_mc._visible = false;
		} else {
			scroll_mc._visible = true;
		}
		//populateList();
	}
	function onEnterFrame() {
		//trace("enterframe! "+scroll_base+" "+scroll_mc.scroll_fraction+" "+scroll_height);
		this._y = scroll_base-(scroll_mc.scroll_fraction*scroll_height);
	}
	function populateList() {
		scroll_height = (scroll_txt.textHeight+header_height)-viewable_height;
		scroll_mc.resetSlider();
		if (scroll_height<0) {
			scroll_height = 0;
			scroll_mc._visible = false;
		} else {
			scroll_mc._visible = true;
		}
	}
}
