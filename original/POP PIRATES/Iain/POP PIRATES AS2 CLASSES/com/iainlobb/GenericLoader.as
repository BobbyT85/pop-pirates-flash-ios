﻿class com.iainlobb.GenericLoader extends com.iainlobb.ExtendedMC {
	public var percentage_str:String;
	public var bar_mc:MovieClip;
	public var done_frame:Number;
	public function GenericLoader() {
		bar_mc._xscale = 0;
		fadeIn(100);
	}
	public function init(ini_done_frame) {
		done_frame = ini_done_frame;
	}
	public function activate() {
		onEnterFrame = function () {
			var loaded = _root.getBytesLoaded()/_root.getBytesTotal();
			percentage_str = Math.floor(loaded*100)+"%";
			bar_mc._xscale = loaded*100;
			if (loaded == 1 && _root.getBytesTotal()>100) {
				delete this.onEnterFrame;
				loadComplete();
			}
		};
	}
	public function loadComplete() {
		fadeOut(10);
	}
	public function onFadedOut() {
		_root.gotoAndStop(done_frame);
	}
	public function onFadedIn() {
		activate();
	}
}
