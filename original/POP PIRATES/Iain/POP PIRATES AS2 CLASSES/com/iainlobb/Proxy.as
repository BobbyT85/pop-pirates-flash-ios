﻿/*/ ****************** TO USE *********************
	
	someMovieClip.onPress = Proxy.create(this, buttonClicked, someMovieClip, someVar);
	someMovieClip.onRollOver = Proxy.create(this, buttonOver, someMovieClip);
	someMovieClip.onRollOut = Proxy.create(this, buttonOut, someMovieClip);
	
	public function buttonClicked(mc:MovieClip, someVar:Number) {
		mc.gotoAndStop("clicked);
		trace(someVar);
	}
	public function buttonOver(mc:MovieClip) {
		mc.gotoAndStop("over);
	}
	public function buttonOut(mc:MovieClip) {
		mc.gotoAndStop("out);
	}
	
//*/
class com.iainlobb.Proxy {

  public static function create(oTarget:Object, fFunction:Function):Function {
	//trace("create!")
    var aParameters:Array = new Array();
    for(var i:Number = 2; i < arguments.length; i++) {
      aParameters[i - 2] = arguments[i];
    }

    var fProxy:Function = function():Void {
      var aActualParameters:Array = arguments.concat(aParameters);
      fFunction.apply(oTarget, aActualParameters);
    };

    return fProxy;

  }

}