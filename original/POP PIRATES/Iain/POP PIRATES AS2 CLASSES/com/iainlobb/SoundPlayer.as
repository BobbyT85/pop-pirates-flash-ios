﻿//stop();
import com.iainlobb.Proxy;
class com.iainlobb.SoundPlayer extends MovieClip {
	var normal_volume:Number = 100;
	var all_sounds:Sound;
	var sound_1:Sound;
	var sound_2:Sound;
	var sound_3:Sound;
	var sound_4:Sound;
	var sound_5:Sound;
	var sound_6:Sound;
	var sound_7:Sound;
	function onLoad() {
		all_sounds = new Sound(_root);
	}
	function stopSounds() {
		all_sounds.setVolume(0);
	}
	function restoreSounds() {
		all_sounds.setVolume(normal_volume);
	}
	function setAudioVolume(the_volume){
		all_sounds.setVolume(the_volume);
	}
	function startSound(channel, linkage, vol, loop, offset) {
		if (offset == undefined){
			offset = 0
		}
		trace("startSound! "+[channel, linkage, vol, loop])
		stopSound(channel, linkage);
		var sound_mc:MovieClip = this.createEmptyMovieClip("sound_"+channel+"_mc", channel);
		this["sound_"+channel] = new Sound(sound_mc);
		var the_sound:Sound = this["sound_"+channel];
		the_sound.attachSound(linkage);
		the_sound.setVolume(vol);
		the_sound.stop(linkage);
		the_sound.start(offset, loop);
	}
	function fadeInSound(channel, max_volume, inc) {
		var the_sound:Sound = this["sound_"+channel];
		var sound_mc:MovieClip = this["sound_"+channel+"_mc"];
		sound_mc.my_sound = the_sound;
		sound_mc.inc = inc;
		sound_mc.onEnterFrame = function() {
			var current_vol:Number = this.my_sound.getVolume();
			if (current_vol<max_volume) {
				this.my_sound.setVolume(current_vol+this.inc);
			} else {
				delete this.onEnterFrame;
			}
		};
	}
	function fadeOutSound(channel, inc, linkage) {
		var the_sound:Sound = this["sound_"+channel];
		var sound_mc:MovieClip = this["sound_"+channel+"_mc"];
		sound_mc.my_sound = the_sound;
		sound_mc.inc = inc;
		sound_mc.channel = channel;
		sound_mc.linkage = linkage;
		sound_mc.onEnterFrame = function() {
			var current_vol:Number = this.my_sound.getVolume();
			if (current_vol>0) {
				this.my_sound.setVolume(current_vol-this.inc);
			} else {
				delete this.onEnterFrame;
				stopSound(this.cahnnel, this.linkage);
			}
		};
	}
	function stopSound(channel, linkage) {
		var the_sound:Sound = this["sound_"+channel];
		the_sound.stop(linkage);
		var sound_mc:MovieClip = this["sound_"+channel+"_mc"];
		sound_mc.removeMovieClip();
	}
	function changeVolume(channel, vol) {
		var the_sound:Sound = this["sound_"+channel];
		the_sound.setVolume(vol);
	}
}
