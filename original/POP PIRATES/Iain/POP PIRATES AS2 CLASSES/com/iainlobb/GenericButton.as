﻿import com.iainlobb.ExtendedMC;
class com.iainlobb.GenericButton extends ExtendedMC {
	public var released:Function;
	public var pressed:Function;
	public var info_object:Object;
	public function GenericButton() {
	}
	public function init(ini_info_object) {
		trace("init "+ini_info_object);
		info_object = ini_info_object;
	}
	function onPress() {
		dispatchEvent({type:"onPress", target:this, info_object:info_object});
		gotoAndStop("down");
		pressed();
	}
	function onRelease() {
		//trace("release");
		dispatchEvent({type:"onRelease", target:this, info_object:info_object});
		gotoAndStop("over");
		released();
	}
	function onReleaseOutside() {
		gotoAndStop("out");
	}
	function onRollOver() {
		gotoAndStop("over");
	}
	function onRollOut() {
		dispatchEvent({type:"onRollOut", target:this, info_object:info_object});
		gotoAndStop("out");
	}
	function onDragOver() {
	}
	function onDragOut() {
		dispatchEvent({type:"onDragOut", target:this, info_object:info_object});
		gotoAndStop("out");
	}
}
