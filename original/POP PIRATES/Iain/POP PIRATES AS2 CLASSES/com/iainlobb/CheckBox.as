﻿import com.iainlobb.ExtendedMC;
class com.iainlobb.CheckBox extends ExtendedMC {
	var is_selected:Boolean;
	var x_mc:MovieClip;
	function CheckBox() {
		is_selected = false;
		x_mc._visible = false;
	}
	function onRelease() {
		x_mc._visible = !x_mc._visible;
		is_selected = !is_selected;
	}
}
