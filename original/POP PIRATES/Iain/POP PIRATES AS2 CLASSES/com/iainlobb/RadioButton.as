﻿import com.iainlobb.ExtendedMC;
class com.iainlobb.RadioButton extends ExtendedMC {
	var is_selected:Boolean;
	var x_mc:MovieClip;
	var group_array:Array;
	var my_value:String;
	function RadioButton() {
		is_selected = false;
		x_mc._visible = false;
	}
	function onRelease() {
		for (var i = 0; i<group_array.length; i++) {
			if (group_array[i] != this) {
				group_array[i].unselect();
			}
		}
		x_mc._visible = true;
		is_selected = true;
	}
	function init(ini_value, ini_group_array, ini_is_selected) {
		my_value = ini_value;
		group_array = ini_group_array;
		is_selected = ini_is_selected
		x_mc._visible = is_selected;
	}
	function select() {
	}
	function unselect() {
		x_mc._visible = false;
		is_selected = false;
	}
}
