﻿import com.iainlobb.ExtendedMC;
import com.iainlobb.Proxy;
class com.iainlobb.ScrollBar extends ExtendedMC {
	public var slider_mc:MovieClip;
	public var up_btn:Button;
	public var down_btn:Button;
	public var scroll_controller:MovieClip;
	public var scroll_height:Number;
	public var scroll_fraction:Number;
	public var sensitivity:Number;
	public function ScrollBar() {
	}
	public function init(ini_scroll_height, ini_sensitivity) {
		scroll_height = ini_scroll_height;
		if (ini_sensitivity != undefined) {
			sensitivity = ini_sensitivity;
		} else {
			sensitivity = 1;
		}
		scroll_controller = createEmptyMovieClip("scroll_controller", 1000);
		slider_mc.onPress = function() {
			this.startDrag(false, 2, 0, 2, _parent.scroll_height);
			_parent.scroll_controller.onEnterFrame = function() {
				_parent.updateValue();
			};
		};
		slider_mc.onRelease = function() {
			_parent.deactivate();
			this.stopDrag();
		};
		slider_mc.onReleaseOutside = function() {
			_parent.deactivate();
			this.stopDrag();
		};
		up_btn.onPress = function() {
			_parent.scroll_controller.onEnterFrame = function() {
				var tempy = _parent.slider_mc._y-_parent.sensitivity;
				if (tempy<0) {
					_parent.slider_mc._y = 0;
				} else {
					_parent.slider_mc._y = tempy;
				}
				_parent.updateValue();
			};
		};
		up_btn.onRelease = function() {
			_parent.deactivate();
		};
		up_btn.onDragOut = function() {
			_parent.deactivate();
		};
		down_btn.onPress = function() {
			_parent.scroll_controller.onEnterFrame = function() {
				var tempy = _parent.slider_mc._y+_parent.sensitivity;
				if (tempy>_parent.scroll_height) {
					_parent.slider_mc._y = _parent.scroll_height;
				} else {
					_parent.slider_mc._y = tempy;
				}
				_parent.updateValue();
			};
		};
		down_btn.onRelease = function() {
			_parent.deactivate();
		};
		down_btn.onDragOut = function() {
			_parent.deactivate();
		};
		startWheel()
	}
	function deactivate() {
		delete scroll_controller.onEnterFrame;
	}
	function updateValue() {
		this.scroll_fraction = slider_mc._y/scroll_height;
	}
	function resetSlider() {
		slider_mc._y = 0;
		updateValue();
	}
	function wheelScroll(delta) {
		trace("wheelScroll "+delta);
		var tempy = slider_mc._y-((delta/scroll_height)*100);
		if (tempy<slider_mc.top_y) {
			slider_mc._y = slider_mc.top_y;
		} else if (tempy>slider_mc.bottom_y) {
			slider_mc._y = slider_mc.bottom_y;
		} else {
			slider_mc._y = tempy;
		}
		updateValue();
	}
	// in section:
	function startWheel() {
		var mouse_listener = new Object();
		mouse_listener.onMouseWheel = Proxy.create(this, onWheel);
		Mouse.addListener(mouse_listener);
	}
	function onWheel(delta) {
		trace("wheel "+delta);
		wheelScroll(delta);
	}
}
