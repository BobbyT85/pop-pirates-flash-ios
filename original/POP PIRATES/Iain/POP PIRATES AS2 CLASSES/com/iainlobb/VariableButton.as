﻿import com.iainlobb.ExtendedMC;
class com.iainlobb.VariableButton extends ExtendedMC {
	public var label_txt:TextField;
	public var over_transform;
	public var off_transform;
	public var down_transform;
	public var info_object:Object;
	public var my_colour:Color;
	public function VariableButton() {
		label_txt.autoSize = true;
		my_colour = new Color(this);
	}
	public function init(ini_info_object, ini_off_transform, ini_over_transform, ini_down_transform) {
		trace("init "+[ini_info_object, ini_off_transform, ini_over_transform, ini_down_transform]);
		info_object = ini_info_object;
		over_transform = ini_over_transform;
		off_transform = ini_off_transform;
		down_transform = ini_down_transform;
		tintOff();
	}
	function onPress() {
		dispatchEvent({type:"onPress", target:this, info_object:info_object});
		tintDown();
		//pressed();
	}
	function onRelease() {
		//trace("release");
		dispatchEvent({type:"onRelease", target:this});
		trace("yo!");
		//tintOff();
		//released();
	}
	function onReleaseOutside() {
		tintOff();
	}
	function onRollOver() {
		tintOver();
	}
	function onRollOut() {
		tintOff();
	}
	function onDragOver() {
	}
	function onDragOut() {
		dispatchEvent({type:"onDragOut", target:this, info_object:info_object});
		tintOff();
	}
	function tintOff() {
		my_colour.setRGB(off_transform);
		trace("tintOff "+off_transform);
	}
	function tintDown() {
		trace("tintDown "+down_transform);
		my_colour.setRGB(down_transform);
	}
	function tintOver() {
		trace("tintOver "+over_transform);
		my_colour.setRGB(over_transform);
	}
}
