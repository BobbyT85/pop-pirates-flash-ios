﻿import mx.events.EventDispatcher;
import mx.utils.Delegate;
import com.iainlobb.GenericTimer;
//import com.iainlobb.Proxy;
class com.iainlobb.ExtendedMC extends MovieClip {
	public var fade_speed:Number;
	public var Wallpaper_timer:Function;
	public var onFadedOut:Function;
	public var onFadedIn:Function;
	public var dispatchEvent:Function;
	public var addEventListener:Function;
	public var removeEventListener:Function;
	public var old_colour_data:Object;
	public var new_colour_data:Object;
	public function ExtendedMC() {
		EventDispatcher.initialize(this);
	}
	public function init() {
	}
	public function fadeIn(speed) {
		this._visible = true;
		if (speed == undefined) {
			speed = 5;
		}
		fade_speed = speed;
		_alpha = 0;
		onEnterFrame = function () {
			if (_alpha<100) {
				_alpha += fade_speed;
			} else {
				delete this.onEnterFrame;
				dispatchEvent({type:"onFadedIn", target:this});
				onFadedIn();
			}
		};
	}
	public function fadeScaleIn(speed) {
		this._visible = true;
		if (speed == undefined) {
			speed = 5;
		}
		fade_speed = speed;
		_alpha = 0;
		_xscale = 0;
		_yscale = 0;
		onEnterFrame = function () {
			if (_alpha<100) {
				_alpha += fade_speed;
				_xscale += fade_speed;
				_yscale += fade_speed;
			} else {
				delete this.onEnterFrame;
				onFadedIn();
			}
		};
	}
	public function fadeOut(speed) {
		if (speed != undefined) {
			fade_speed = speed;
		} else {
			fade_speed = 5;
		}
		onEnterFrame = function () {
			if (_alpha>0) {
				_alpha -= fade_speed;
			} else {
				this._visible = false;
				delete this.onEnterFrame;
				dispatchEvent({type:"onFadedOut", target:this});
				onFadedOut();
			}
		};
	}
	function burnIn(ini_speed) {
		var my_color:Color = new Color(this);
		var my_color_transform:Object = new Object();
		var color_var = 200;
		var speed = ini_speed;
		my_color_transform = {ra:100, rb:color_var, ga:100, gb:color_var, ba:100, bb:color_var, aa:100, ab:0};
		my_color.setTransform(my_color_transform);
		this.onEnterFrame = function() {
			color_var = color_var-speed;
			if (color_var<0) {
				color_var = 0;
				delete this.onEnterFrame;
				dispatchEvent({type:"onBurnedIn", target:this});
			}
			my_color_transform = {ra:100, rb:color_var, ga:100, gb:color_var, ba:100, bb:color_var, aa:100, ab:0};
			my_color.setTransform(my_color_transform);
		};
	}
	function burnOut(ini_speed) {
		var my_color:Color = new Color(this);
		var my_color_transform:Object = new Object();
		var color_var = 0;
		var speed = ini_speed;
		my_color_transform = {ra:100, rb:color_var, ga:100, gb:color_var, ba:100, bb:color_var, aa:100, ab:0};
		my_color.setTransform(my_color_transform);
		this.onEnterFrame = function() {
			color_var = color_var+speed;
			if (color_var>200) {
				color_var = 0;
				delete this.onEnterFrame;
				fadeOut(20)
				dispatchEvent({type:"onBurnedIn", target:this});
			}
			my_color_transform = {ra:100, rb:color_var, ga:100, gb:color_var, ba:100, bb:color_var, aa:100, ab:0};
			my_color.setTransform(my_color_transform);
		};
	}
}
