﻿import com.iainlobb.ExtendedMC;
import com.iainlobb.Proxy;
class com.iainlobb.DropMenuScrollBar extends ExtendedMC {
	public var slider_mc:MovieClip;
	public var bg_mc:MovieClip;
	public var up_btn:Button;
	public var down_btn:Button;
	public var scroll_controller:MovieClip;
	public var scroll_height:Number;
	public var scroll_fraction:Number;
	public var sensitivity:Number;
	var scroll_area:Number;
	public function DropMenuScrollBar() {
	}
	public function init(ini_scroll_height, ini_sensitivity, down_btn_y) {
		if (down_btn_y == undefined){
			down_btn_y = 2
		}
		scroll_height = ini_scroll_height;
		bg_mc._height = scroll_height;
		scroll_fraction = 0;
		down_btn._y = scroll_height-down_btn._height+down_btn_y;
		if (ini_sensitivity != undefined) {
			sensitivity = ini_sensitivity;
		} else {
			sensitivity = 1;
		}
		scroll_controller = createEmptyMovieClip("scroll_controller", 1000);
		slider_mc.top_y = 10;
		slider_mc._y = 20
		slider_mc._x = -6
		slider_mc.bottom_y = scroll_height-24;
		scroll_area = slider_mc.bottom_y-slider_mc.top_y;
		slider_mc.onPress = function() {
			Selection.setFocus(this);
			this.startDrag(false, -6, this.top_y, -6, this.bottom_y);
			_parent.scroll_controller.onEnterFrame = function() {
				_parent.updateValue();
			};
		};
		bg_mc.onPress = function() {
			Selection.setFocus(this);
			var tempy = this._ymouse*(this._yscale/100)-5;
			if (tempy<_parent.slider_mc.top_y) {
				_parent.slider_mc._y = _parent.slider_mc.top_y;
			} else if (tempy>_parent.slider_mc.bottom_y) {
				_parent.slider_mc._y = _parent.slider_mc.bottom_y;
			} else {
				_parent.slider_mc._y = tempy;
			}
			_parent.updateValue();
		};
		slider_mc.onRelease = function() {
			_parent.deactivate();
			this.stopDrag();
		};
		slider_mc.onReleaseOutside = function() {
			_parent.deactivate();
			this.stopDrag();
		};
		up_btn.onPress = function() {
			_parent.scroll_controller.onEnterFrame = function() {
				var tempy = _parent.slider_mc._y-_parent.sensitivity;
				if (tempy<_parent.slider_mc.top_y) {
					_parent.slider_mc._y = _parent.slider_mc.top_y;
				} else if (tempy>_parent.slider_mc.bottom_y) {
					_parent.slider_mc._y = _parent.slider_mc.bottom_y;
				} else {
					_parent.slider_mc._y = tempy;
				}
				_parent.updateValue();
			};
		};
		up_btn.onRelease = function() {
			_parent.deactivate();
		};
		up_btn.onDragOut = function() {
			_parent.deactivate();
		};
		down_btn.onPress = function() {
			_parent.scroll_controller.onEnterFrame = function() {
				var tempy = _parent.slider_mc._y+_parent.sensitivity;
				if (tempy<_parent.slider_mc.top_y) {
					_parent.slider_mc._y = _parent.slider_mc.top_y;
				} else if (tempy>_parent.slider_mc.bottom_y) {
					_parent.slider_mc._y = _parent.slider_mc.bottom_y;
				} else {
					_parent.slider_mc._y = tempy;
				}
				_parent.updateValue();
			};
		};
		down_btn.onRelease = function() {
			_parent.deactivate();
		};
		down_btn.onDragOut = function() {
			_parent.deactivate();
		};
		startWheel()
	}
	function deactivate() {
		delete scroll_controller.onEnterFrame;
	}
	function updateValue() {
		this.scroll_fraction = (slider_mc._y-slider_mc.top_y)/scroll_area;
	}
	function resetSlider() {
		slider_mc._y = slider_mc.top_y;
		updateValue();
	}
	function wheelScroll(delta) {
		trace("wheelScroll "+delta);
		var tempy = slider_mc._y-((((delta*_parent.sensitivity)/4)/scroll_height)*100);
		if (tempy<slider_mc.top_y) {
			slider_mc._y = slider_mc.top_y;
		} else if (tempy>slider_mc.bottom_y) {
			slider_mc._y = slider_mc.bottom_y;
		} else {
			slider_mc._y = tempy;
		}
		updateValue();
	}
	// in section:
	function startWheel() {
		var mouse_listener = new Object();
		mouse_listener.onMouseWheel = Proxy.create(this, onWheel);
		Mouse.addListener(mouse_listener);
	}
	function onWheel(delta) {
		trace("wheel "+delta);
		wheelScroll(delta);
	}
}
