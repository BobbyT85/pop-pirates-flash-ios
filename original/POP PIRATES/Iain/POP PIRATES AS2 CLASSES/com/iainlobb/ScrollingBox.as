﻿class com.iainlobb.ScrollingBox extends MovieClip {
	var scroll_mc:MovieClip;
	var scroll_height:Number;
	var scroll_base:Number;
	var viewable_height:Number;
	var scroll_txt:TextField;
	var title_txt:TextField;
	public var header_height:Number;
	public var image_mc:MovieClip;
	function ScrollingBox() {
		scroll_height = 0;
		header_height = 0;
	}
	function init(ini_scroll_mc, ini_viewable_height, ini_scroll_base, ini_scroll_height) {
		scroll_mc = ini_scroll_mc;
		viewable_height = ini_viewable_height;
		scroll_base = ini_scroll_base;
		scroll_height = ini_scroll_height;
		this.onEnterFrame = function() {
			this._y = scroll_base-(scroll_mc.scroll_fraction*scroll_height);
		};
		scroll_mc.resetSlider();
	}
}
