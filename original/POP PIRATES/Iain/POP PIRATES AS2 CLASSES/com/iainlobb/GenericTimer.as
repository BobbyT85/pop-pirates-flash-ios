﻿class com.iainlobb.GenericTimer extends com.iainlobb.ExtendedMC {
	public var ticks:Number = 0;
	public var delay:Number = 0;
	public function GenericTimer() {
	}
	public function startTimer(ini_delay) {
		delay = ini_delay;
		onEnterFrame = function () {
			ticks++;
			if (ticks>delay) {
				dispatchEvent({type:"onComplete", target:this});
				this.removeMovieClip();
			}
		};
	}
}
