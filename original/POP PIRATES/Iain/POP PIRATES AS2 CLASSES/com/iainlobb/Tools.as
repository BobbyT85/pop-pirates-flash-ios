﻿class com.iainlobb.Tools {
	static function getPlusMinus():Number {
		var choice:Number = random(2);
		if (choice == 0) {
			return -1;
		} else {
			return 1;
		}
	}
	static function getPlusMinusZero(num:Number):Number {
		if (num == 0) {
			return 0;
		} else if (num>0) {
			return 1;
		} else {
			return -1;
		}
	}
	//static function checkFString():Number {
	//}
	static function removeFromArray(the_array:Array, val):Void {
		var array_length:Number = the_array.length;
		for (var i:Number = 0; i<array_length; i++) {
			if (the_array[i] == val) {
				var my_pos = i;
				break;
			}
		}
		the_array.splice(my_pos, 1);
	}
	static function removeObjectFromArray(the_array:Array, prop, val) {
		var array_length:Number = the_array.length;
		for (var a:Number = 0; a<array_length; a++) {
			if (the_array[a][prop] == val) {
				var mePos = a;
				break;
			}
		}
		the_array.splice(mePos, 1);
		return;
	}
	static function checkEmail(email_str:String):Boolean {
		if (email_str.length>=7) {
			if (email_str.indexOf("@")>0) {
				if ((email_str.indexOf("@")+2)<email_str.lastIndexOf(".")) {
					if (email_str.lastIndexOf(".")<(email_str.length-2)) {
						return (true);
					}
				}
			}
		}
		return (false);
	}
	static function checkDayInMonth(day:Number, month:Number):Boolean {
		if (isNaN(day) || isNaN(month)) {
			return false;
		}
		var days_in_months_array = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		if (day>0 && day<=days_in_months_array[month-1]) {
			return true;
		} else {
			return false;
		}
	}
	static function calculateAge(day:Number, month:Number, year:Number):Number {
		if (isNaN(day) || isNaN(month) || isNaN(year)) {
			return -1;
		}
		var today_date:Date = new Date();
		var today_day:Number = today_date.getDate();
		var today_month:Number = today_date.getMonth()+1;
		var today_year:Number = today_date.getFullYear();
		var age = today_year-year;
		if (month>today_month) {
			age--;
		} else if (month == today_month) {
			if (day>today_day) {
				age--;
			}
		}
		return age;
	}
	static function roundToDecimalPlaces(num, num_dps:Number):String {
		var num_str:String = num+"";
		//if (num_str.length > 
		var dp_pos = num_str.indexOf(".");
		if (dp_pos == -1) {
			return num_str;
		}
		var rounded_str:String = num_str.slice(0, dp_pos+num_dps+1);
		return rounded_str;
	}
	static function getAngle(x1:Number, y1:Number, x2:Number, y2:Number):Number {
		var angle = Math.atan2(y2-y1, x2-x1)*180/Math.PI;
		var ang = 360-(((angle %= 360)<0) ? angle+360 : angle);
		return (ang == 360) ? 0 : ang;
	}
	static function convertDeg(deg:Number, out_str:String):Number {
		if (out_str == "rad") {
			return deg*Math.PI/180;
		} else if (out_str == "multPI") {
			return deg/180;
		}
	}
}
