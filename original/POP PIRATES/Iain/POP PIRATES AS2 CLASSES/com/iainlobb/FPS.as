﻿import com.iainlobb.Proxy;
class com.iainlobb.FPS extends MovieClip {
	// Symbols
	private var fps_txt:TextField;
	// Variables
	private var old_time:Number = 0;
	private var new_time:Number = 0;
	private var fps_array:Array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	// Functions
	private function FPS() {
		trace("FPS Created, instance: "+this);
	}
	private function onEnterFrame():Void {
		old_time = new_time;
		new_time = getTimer();
		var my_fps:Number = 1000/(new_time-old_time);
		fps_array.unshift(my_fps);
		fps_array.pop();
		var total:Number = 0;
		for (var i:Number = 0; i<fps_array.length; i++) {
			total += fps_array[i];
		}
		var real_fps:Number = Math.round(total/20);
		fps_txt.text = String(real_fps);
	}
}
