﻿import com.iainlobb.ExtendedMC;
class com.iainlobb.DropMenu extends ExtendedMC {
	public var items_array:Array;
	public var out_btn:Button;
	public var main_btn:Button;
	public var selected_str:String = "";
	public var selected_num:Number;
	public var bottom_mc:MovieClip;
	public var row_height:Number;
	public var base_height:Number;
	public var num_display_items;
	public var scrollbar_mc:MovieClip;
	public var mask_mc:MovieClip;
	public var holder_mc:MovieClip;
	public var variant_str:String;
	public var width:Number;
	public var sensitivity:Number;
	public function DropMenu() {
		row_height = 15;
		base_height = 14;
	}
	public function init(ini_array:Array, ini_num_display_items, ini_variant_str, ini_width, ini_sensitivity) {
		sensitivity = ini_sensitivity;
		num_display_items = ini_num_display_items;
		items_array = ini_array;
		width = ini_width;
		if (ini_variant_str != undefined) {
			variant_str = ini_variant_str;
		} else {
			variant_str = "";
		}
		inactive();
	}
	function inactive() {
		main_btn.onPress = function() {
			_parent.activate();
			Selection.setFocus(this);
		};
	}
	function deactivate() {
		mask_mc.removeMovieClip();
		holder_mc.removeMovieClip();
		bottom_mc.removeMovieClip();
		scrollbar_mc._visible = false;
		gotoAndStop(1);
		inactive();
	}
	function setItem(num:Number) {
		selected_str = items_array[num];
		selected_num = num;
		deactivate();
		dispatchEvent({type:"onChosen", target:this});
	}
	function activate() {
		this.swapDepths(1);
		gotoAndStop(2);
		if (num_display_items<items_array.length) {
		}
		main_btn.onPress = function() {
			_parent.deactivate();
		};
		out_btn.useHandCursor = false;
		out_btn.onPress = function() {
			_parent.deactivate();
		};
		mask_mc = this.attachMovie("DropMenuMask", "mask_mc", 9999, {_x:0, _y:20.1, _width:400, _height:(num_display_items*15)-1});
		holder_mc = this.attachMovie("ScrollingBox", "holder_mc", 1, {_x:0, _y:19});
		holder_mc.setMask(mask_mc);
		for (var i:Number = 0; i<items_array.length; i++) {
			var new_item_mc = holder_mc.attachMovie("DropMenuItem"+variant_str, "item"+i+"_mc", i, {_y:15*i});
			new_item_mc.val_txt.text = items_array[i];
			new_item_mc.num = i;
			new_item_mc.onPress = function() {
				_parent._parent.setItem(this.num);
			};
			new_item_mc.onRollOver = function() {
				this.gotoAndStop(2);
			};
			new_item_mc.onRollOut = function() {
				this.gotoAndStop(1);
			};
			new_item_mc.onDragOut = function() {
				this.gotoAndStop(1);
			};
		}
		holder_mc.onEnterFrame = function() {
		};
		
		
		scrollbar_mc = attachMovie("drop_down_scroller", "scrollbar_mc", 999, {_y:19, _x:width-10});
		scrollbar_mc.init(num_display_items*15, sensitivity);
		holder_mc.init(scrollbar_mc, num_display_items*15, 19, (items_array.length*15)-(num_display_items*15));
		bottom_mc = this.attachMovie("DropMenuItemBottom", "bottom_mc", 99999, {_x:1.5, _y:base_height+(row_height*num_display_items)+6});
		bottom_mc._width = width;
		bottom_mc._height = 1;
		if (num_display_items>=items_array.length) {
			scrollbar_mc._visible = false;
		}
		//Selection.setFocus(this);
	}
}
