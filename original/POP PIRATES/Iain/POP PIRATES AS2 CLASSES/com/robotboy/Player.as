﻿import com.iainlobb.Proxy;
class com.robotboy.Player extends MovieClip {
	// Symbols
	private var hit_mc:MovieClip;
	private var sonar_mc:MovieClip;
	private var glow_mc:MovieClip;
	private var life_mc:MovieClip;
	// Variables
	private var is_alive:Boolean;
	private var firing:Boolean = false;
	private var is_coloured:Boolean;
	//
	private var weapon_array:Array;
	private var charge_array:Array;
	//
	private var timer:Number = 0;
	private var my_sound_channel:Number = 0;
	private var lives:Number = 5;
	private var life_timer:Number;
	private var colour_timer:Number;
	private var basic_fire_rate:Number = 8;
	private var score:Number;
	private var player_speed:Number = 5;
	//
	private var my_color:Color;
	// Functions
	private function Player() {
		trace("Player Created, instance: "+this);
	}
	private function checkInvulnerability(vun_timer_mc:MovieClip):Void {
		timer++;
		if (timer>50) {
			is_alive = true;
			vun_timer_mc.removeMovieClip();
		}
	}
	function startMouse() {
		//MOUSE CONTROL
		delete _global.mouse_listener;
		_global.mouse_listener = new Object();
		_global.mouse_listener.onMouseDown = Proxy.create(this, startShooting);
		_global.mouse_listener.onMouseUp = Proxy.create(this, stopShooting);
		Mouse.addListener(_global.mouse_listener);
	}
	private function onLoad():Void {
		startMouse();
		createEmptyMovieClip("life_mc", 2);
		weapon_array = [0, 1, 0, 0, 0, 0, 0, 5];
		charge_array = [0, 0, 0, 0, 0, 0, 0];
		_parent.levelUp(this, 1);
		_parent.active_players_array.push(this);
		lives = 5;
		is_alive = false;
		var vun_timer_mc:MovieClip = createEmptyMovieClip("vun_timer_mc", 27);
		vun_timer_mc.timer = 0;
		vun_timer_mc.onEnterFrame = Proxy.create(this, checkInvulnerability, vun_timer_mc);
		//
		hit_mc._visible = false;
		sonar_mc._visible = false;
		glow_mc._visible = false;
		my_sound_channel = 8;
		// MOVEMENT
		this.onEnterFrame = function() {
			if (firing) {
				fireGun();
			}
			for (var i:Number = 1; i<this.charge_array.length; i++) {
				this.charge_array[i] += 1;
			}
			// tx is the target x for easing, etc
			this.tx = _parent._xmouse;
			this.ty = _parent._ymouse;
			if (this.tx>_parent.right_limit-30) {
				this.tx = _parent.right_limit-30;
			} else if (this.tx<30) {
				this.tx = 30;
			}
			if (this.ty>_parent.bottom_limit-40) {
				this.ty = _parent.bottom_limit-40;
			} else if (this.ty<_parent.top_limit) {
				this.ty = _parent.top_limit;
			}
			var dx = (this.tx-this._x)/player_speed;
			this._x += dx;
			var dy = (this.ty-this._y)/player_speed;
			this._y += dy;
			_parent.shadow_mc._x = 40+this._x;
			_parent.shadow_mc._y = 40+this._y;
			var boostHyp = Math.sqrt((dx*dx)+(dy*dy));
			if (boostHyp>20) {
				boostHyp = 20;
			}
			this.boost2_mc._xscale = 30+random(30)+(boostHyp*8);
			this.boost2_mc._yscale = 30+random(30)+(boostHyp*10);
			var glowframe = this.glow_mc.glow_mc._currentframe;
			if (dx>2) {
				this.fighter_mc.gotoAndStop(2);
				this.hit_mc.gotoAndStop(2);
				this.glow_mc.gotoAndStop(2);
				this.sonar_mc.gotoAndStop(2);
			} else if (dx<-2) {
				this.fighter_mc.gotoAndStop(3);
				this.hit_mc.gotoAndStop(3);
				this.glow_mc.gotoAndStop(3);
				this.sonar_mc.gotoAndStop(3);
			} else {
				this.fighter_mc.gotoAndStop(1);
				this.hit_mc.gotoAndStop(1);
				this.glow_mc.gotoAndStop(1);
				this.sonar_mc.gotoAndStop(1);
			}
			this.glow_mc.glow_mc.gotoAndPlay(glowframe);
			this.sonar_mc.sonar_mc.gotoAndPlay(glowframe);
		};
	}
	function flashWhite() {
		my_color = new Color(this);
		my_color.setRGB(0xffffff);
	}
	function resetColour() {
		my_color.setTransform({ra:100, rb:0, ga:100, gb:0, ba:100, bb:0, aa:100, ab:0});
		delete my_color;
	}
	function hit() {
		// SO CANT BE KILLED TWICE IN A ROW:
		if (is_alive) {
			_parent.sound_player_mc.startSound(8, "powerdown", 100, 1);
			_parent.sound_player_mc.startSound(9, "bomb", 100, 1);
			_parent.createExplosion(_x, _y);
			is_alive = false;
			// LIVES
			lives--;
			weapon_array[7]--;
			if (lives<1) {
				//
				_x = -99999;
				_y = -99999;
				_visible = false;
				//
				_parent.killPlayer();
				delete this.onEnterFrame;
			}
			removePower();
			//FLASHING SHIP
			flashWhite();
			life_timer = 0;
			colour_timer = 0;
			life_mc.onEnterFrame = Proxy.create(this, lifeTimerUpdate);
		}
	}
	function lifeTimerUpdate() {
		life_timer++;
		colour_timer++;
		if (colour_timer>3) {
			is_coloured = !is_coloured;
			colour_timer = 0;
			if (is_coloured) {
				makeTransparent();
			} else {
				resetColour();
			}
		}
		// RESTORE SHIP                                                                                                                                                                                                                                                                                                                                                                                                                        
		if (life_timer>60) {
			resetColour();
			life_timer = 0;
			is_alive = true;
			delete life_mc.onEnterFrame;
		}
	}
	function removePower() {
		var the_power_array:Array = [];
		for (var i:Number = 2; i<=6; i++) {
			if (weapon_array[i] != 0) {
				the_power_array.push(i);
			}
		}
		var the_power:Number = the_power_array[random(the_power_array.length)];
		if (the_power != 5) {
			weapon_array[the_power] = 0;
			if (the_power == 6) {
				sonar_mc._visible = false;
				glow_mc._visible = false;
			}
		} else {
			var num_orbs:Number = weapon_array[the_power];
			for (var i:Number = 0; i<num_orbs; i++) {
				_parent.takeOrb();
				weapon_array[5]--;
			}
		}
	}
	function makeTransparent() {
		my_color = new Color(this);
		my_color.setTransform({ra:100, rb:0, ga:100, gb:0, ba:100, bb:0, aa:30, ab:0});
	}
	// ADD A NEW WEAPON / POWER
	function levelUp(level:Number):Void {
		_parent.sound_player_mc.startSound(6, "powerup"+level, 200, 1);
		// ADD POWER UP - DO NOT EXCEED 10
		if (weapon_array[level]<10) {
			weapon_array[level] += 1;
			// SHOW MESSAGE
			if (level != 1 && level != 0) {
				_parent.showPickUp(level);
			}
			// ADD ORB   
			if (level == 5) {
				_parent.addOrb();
			}
			// ADD LIFE   
			if (level == 7) {
				lives++;
			}
			// SHOW MAGNATRON ANIMATIONS   
			if (level == 6) {
				sonar_mc._visible = true;
				glow_mc._visible = true;
			}
		} else {
			// GIVE POINTS IF PLAYER IS TOO JUICED
			score += 1000;
		}
	}
	function fireGun() {
		// STANDARD WEAPON (DOUBLE BLASTER):
		if (weapon_array[1]>0) {
			if (charge_array[1]>basic_fire_rate) {
				_parent.fireBullet(15, 0, 15, 1, 1);
				_parent.fireBullet(-15, 0, 15, 1, 1);
				charge_array[1] = 0;
				_parent.sound_player_mc.startSound(my_sound_channel+1, "shoot1", 20, 1);
			}
		}
		// FIREWALL   
		if (weapon_array[2]>0) {
			if (charge_array[2]>20) {
				var num_flames:Number = weapon_array[2]+1;
				for (var i:Number = 0; i<num_flames; i++) {
					var wedge_size:Number = num_flames/(num_flames-1);
					var fire_direction = (num_flames*-0.5)+(i*wedge_size);
					_parent.fireBullet(0, fire_direction, 10, 0.5, 2);
				}
				charge_array[2] = 0;
				_parent.sound_player_mc.startSound(my_sound_channel+2, "shoot2", 100, 1);
			}
		}
		// FAST MOVING ELECTRIC RAY   
		if (weapon_array[3]) {
			if (charge_array[3]>12-(weapon_array[3])) {
				_parent.fireBullet(0, 0, 20, 0.2, 3);
				charge_array[3] = 0;
				_parent.sound_player_mc.startSound(my_sound_channel+3, "shoot3", 40, 1);
			}
		}
		// HEAT SEEKER   
		if (weapon_array[4]) {
			if (charge_array[4]>26-(weapon_array[4]*2)) {
				_parent.fireHeatSeeker();
				charge_array[4] = 0;
				_parent.sound_player_mc.startSound(my_sound_channel+4, "shoot4", 30, 1);
			}
		}
	}
	function startShooting() {
		firing = true;
	}
	function stopShooting() {
		firing = false;
	}
}
