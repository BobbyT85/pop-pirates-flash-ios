﻿import com.iainlobb.Proxy;
import com.iainlobb.Tools;
class com.robotboy.BadBullet extends MovieClip {
	// Symbols
	private var target_mc:MovieClip;
	// Variables
	private var timer:Number = 0;
	private var dx:Number = 0;
	private var dy:Number = 0;
	private var speed:Number = 0;
	// Functions
	private function BadBullet() {
		trace("BadBullet Created, instance: "+this);
	}
	function init(ini_x:Number, ini_y:Number){
		_x = ini_x;
		_y = ini_y;
		speed = (2+Math.random())*_parent.skill;
		target_mc = _parent.player1_mc;
		var my_angle:Number = 180+(90-Tools.getAngle(this._x, this._y, target_mc._x, target_mc._y));
		_rotation = my_angle;
		var theta:Number = Tools.convertDeg(this._rotation, 'rad');
		dx = Math.sin(theta)*speed;
		dy = Math.cos(theta)*speed;
	}
	function onEnterFrame() {
		timer++;
		if (timer>100) {
			_alpha -= 5;
			if (_alpha<1) {
				_parent.killBullet(this);
			}
		}
		_x -= dx;
		_y += dy;
		if (target_mc.hit_mc.hitTest(this._x, this._y, true)) {
			_parent.hit(target_mc);
			_parent.killBullet(this);
		}
		if (this._y>_parent.bottom_limit+70) {
			_parent.killBullet(this);
		} else if (this._y<-70) {
			_parent.killBullet(this);
		}
		if (this._x>_parent.right_limit+70) {
			_parent.killBullet(this);
		} else if (this._x<-70) {
			_parent.killBullet(this);
		}
	}
}
