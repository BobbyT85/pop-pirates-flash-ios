﻿import com.iainlobb.Proxy;
class com.robotboy.Hunter extends com.robotboy.Enemy {
	// Symbols
	private var target_mc:MovieClip;
	// Variables
	private var timer:Number = 0;
	private var dir:Number = 1;
	private var speed:Number;
	private var skill:Number;
	private var hits:Number;
	private var points:Number = 200;
	private var is_kill_event:Boolean;
	//
	private var explosion_str:String = "mega";
	//
	private var is_prone:Boolean = true;
	// Functions
	private function Hunter() {
		trace("Hunter Created, instance: "+this);
	}
	private function onLoad() {
		//
	}
	private function init(ini_hits:Number, ini_target_mc:MovieClip) {
		hits = ini_hits;
		speed = 1+Math.random();
		target_mc = ini_target_mc;
	}
	function onEnterFrame() {
		//TRACKING MOVEMENT
		var dx:Number = (target_mc._x-this._x)/50;
		var dy:Number = (target_mc._y-this._y)/50;
		if (dir == 1) {
			if (dy<speed) {
				dy = speed*5;
			}
		} else {
			if (dy>speed*dir) {
				dy = speed*dir*5;
			}
		}
		var xdir:Number = (dx>0) ? 1 : -1;
		if (Math.abs(dx)>speed*2) {
			dx = speed*2*xdir;
		}
		_x += dx;
		_y += dy;
		timer++;
		// SHOOT
		if (timer>40-(_parent.skill*5)) {
			_parent.fireEnemyAimed(this)
			timer = 0;
		}
		// TURN AROUND                                                                                                                                                                 
		if (_y>_parent.bottom_limit+300) {
			dir = -1;
			_yscale = -100;
		} else if (_y<-300) {
			_yscale = 100;
			dir = 1;
		}
		if (_parent.player1_mc.hit_mc.hitTest(this._x, this._y, true)) {
			_parent.player1_mc.hit();
		}
	}
}
