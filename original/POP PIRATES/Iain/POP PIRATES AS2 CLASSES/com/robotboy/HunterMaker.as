﻿import com.iainlobb.Proxy;
class com.robotboy.HunterMaker extends MovieClip {
	// Symbols
	private var some_mc:MovieClip;
	// Variables
	private var timer:Number;
	private var delay:Number;
	private var total:Number;
	private var num:Number;
	// Functions
	private function HunterMaker() {
		trace("HunterMaker Created, instance: "+this);
	}
	private function onLoad() {
	}
	public function init(ini_total, ini_delay) {
		total = ini_total;
		delay = ini_delay;
		timer = 0;
		num = 0;
	}
	function onEnterFrame() {
		timer++;
		if (timer>this.delay) {
			var newHunter = _parent.addHunter({_x:random(_parent.right_limit), _y:-100, hits:5*_parent.skill, timer:0});
			if (num == total) {
				newHunter.is_kill_event = true;
			}
			timer = 0;
			num++;
			if (num>total) {
				delete this.onEnterFrame;
				this.removeMovieClip();
			}
		}
	}
}
