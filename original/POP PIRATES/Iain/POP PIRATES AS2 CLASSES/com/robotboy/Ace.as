﻿import com.iainlobb.Proxy;
import com.iainlobb.Tools;
class com.robotboy.Ace extends com.robotboy.Enemy {
	// Symbols
	private var some_mc:MovieClip;
	// Variables
	private var is_prone:Boolean;
	public var is_kill_event:Boolean;
	//
	private var shoot:Number;
	private var fire_rate:Number;
	private var speed:Number;
	private var hits:Number;
	private var points:Number;
	private var timer:Number = 0;
	private var skill:Number;
	private var tx:Number;
	private var ty:Number;
	private var turning_speed:Number;
	//
	private var explosion_str:String;
	// Functions
	private function Ace() {
		trace("Ace Created, instance: "+this);
	}
	private function onLoad() {
		_x = 1000-random(2000);
		_y = -300;
		is_prone = true;
		explosion_str = "mega";
		speed = 3*_parent.skill;
		hits = _parent.skill;
		points = 300;
		turning_speed = 4;
		points = 50;
	}
	private function init() {
	}
	private function onEnterFrame() {
		if (_parent.player1_mc.hit_mc.hitTest(this._x, this._y, true)) {
			_parent.hit(_parent.player1_mc);
		}
		var myAngle = 90-Tools.getAngle(this._x, this._y, _parent.player1_mc._x, _parent.player1_mc._y);
		var t1 = (this._rotation-myAngle);
		var t2 = (360-this._rotation)+myAngle;
		if (Math.abs(t2)>Math.abs(t1)) {
			var t = (myAngle>this._rotation) ? 1 : -1;
			if (Math.abs(t1)>this.turning_speed) {
				this._rotation += this.turning_speed*t;
			}
		} else {
			var t = (myAngle>this._rotation) ? 1 : -1;
			if (Math.abs(t2)>this.turning_speed) {
				this._rotation -= this.turning_speed*t;
			}
		}
		var theta:Number = Tools.convertDeg(this._rotation, 'rad');
		var dx:Number = Math.sin(theta)*speed;
		var dy:Number = Math.cos(theta)*speed;
		_x += dx;
		_y -= dy;
	}
}
