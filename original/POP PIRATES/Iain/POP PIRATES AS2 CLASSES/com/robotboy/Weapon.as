﻿import com.iainlobb.Proxy;
class com.robotboy.Weapon extends MovieClip {
	// Symbols
	// Variables
	// Functions
	private function Weapon() {
		trace("Weapon Created, instance: "+this);
	}
	// REMOVE BULLETS (OR ANYTHING) NO LONGER ON STAGE
	function testEdges() {
		if (_y<-10) {
			remove();
		} else if (_x<-10) {
			remove();
		} else if (_x>_parent.right_limit+30) {
			remove();
		} else if (_y>_parent.bottom_limit+70) {
			remove();
		}
	}
	function remove() {
		delete this.onEnterFrame;
		this.removeMovieClip();
	}
}
