﻿import com.iainlobb.Proxy;
import com.iainlobb.Tools;
class com.robotboy.Worm extends com.robotboy.Enemy {
	// Symbols
	private var head_mc:MovieClip;
	private var rotter_mc:MovieClip;
	// Variables
	private var is_head:Boolean;
	private var is_prone:Boolean;
	public var is_kill_event:Boolean;
	//
	private var shoot:Number;
	private var fire_rate:Number;
	private var turning_speed:Number;
	private var speed:Number;
	private var hits:Number;
	private var points:Number;
	//
	private var explosion_str:String;
	// Functions
	private var my_array:Array;
	private function Worm() {
		trace("Worm Created, instance: "+this);
	}
	private function init(ini_is_head:Boolean) {
		is_head = ini_is_head;
		rotter_mc = createEmptyMovieClip("rotter_mc", 1000);
		//
		_x = random(_parent.right_limit);
		_y = random(50);
		hits = _parent.skill;
		my_array = _parent["wormList"+_parent.num_lists];
		my_array.unshift(this);
		is_prone = true;
		fire_rate = 12;
		points = 100;
		shoot = 0;
		turning_speed = 2;
		speed = 2;
		if (is_head) {
			gotoAndStop(2);
			explosion_str = "mega";
			is_kill_event = true;
		}
	}
	function onEnterFrame() {
		if (is_head) {
			if (my_array.length>1) {
				is_prone = false;
			} else {
				is_prone = true;
			}
			shoot++;
			if (shoot == 4) {
				head_mc.gotoAndStop(1);
			}
			if (shoot>fire_rate) {
				head_mc.gotoAndStop(2);
				_parent.fireEnemyAimed(this);
				fire_rate = (200*(1/_parent.skill))*(0.5+(Math.random()*1.5));
				shoot = 0;
			}
			var myAngle = ((Math.atan2(this._y-_parent.player1_mc._y, this._x-_parent.player1_mc._x)*180)/Math.PI)+90;
			_rotation = myAngle;
			myAngle = this._rotation;
			var t1:Number = (myAngle-rotter_mc._rotation);
			if (myAngle<this.rotter_mc._rotation) {
				var t2 = ((360+myAngle)-this.rotter_mc._rotation);
			} else {
				if (myAngle>0) {
					var t2 = -((180-myAngle)+(180+this.rotter_mc._rotation));
				} else {
					var t2 = (180+myAngle)+(180-this.rotter_mc._rotation);
				}
			}
			if (Math.abs(t2)>Math.abs(t1)) {
				var dir = (t1>0) ? 1 : -1;
				if (Math.abs(t1)>this.turning_speed) {
					this.rotter_mc._rotation += this.turning_speed*dir;
				} else {
					this.rotter_mc._rotation -= t1;
				}
			} else {
				var dir = (t2>0) ? 1 : -1;
				if (Math.abs(t2)>this.turning_speed) {
					this.rotter_mc._rotation += this.turning_speed*dir;
				} else {
					this.rotter_mc._rotation += t2;
				}
			}
			var theta:Number = Tools.convertDeg(rotter_mc._rotation, 'rad');
			var dx = -(Math.sin(theta)*this.speed);
			var dy = (Math.cos(theta)*this.speed);
		} else {
			for (var i:Number = 0; i<my_array.length; i++) {
				if (my_array[i] == this) {
					var my_pos:Number = i;
					break;
				}
			}
			var boss_mc:MovieClip = my_array[my_pos-1];
			var hyp = Math.sqrt(((this._x-boss_mc._x)*(this._x-boss_mc._x))+((this._y-boss_mc._y)*(this._y-boss_mc._y)));
			if (hyp>10) {
				var dx = (boss_mc._x-this._x)/8;
				var dy = (boss_mc._y-this._y)/8;
			}
		}
		this._x += dx;
		this._y += dy;
		if (_parent.player1_mc.hit_mc.hitTest(this._x, this._y, true)) {
			_parent.hit(_parent.player1_mc);
		}
	}
}
