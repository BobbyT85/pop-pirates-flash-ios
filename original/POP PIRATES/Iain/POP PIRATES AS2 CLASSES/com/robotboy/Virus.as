﻿import com.iainlobb.Proxy;
class com.robotboy.Virus extends com.robotboy.Enemy {
	// Symbols
	private var target_mc:MovieClip;
	// Variables
	private var hits:Number;
	private var total_hits:Number;
	private var timer:Number;
	private var points:Number;
	//
	private var is_dropping:Boolean
	private var is_boss:Boolean
	private var is_prone:Boolean;
	private var is_kill_event:Boolean;
	//
	private var explosion_str:String;
	// Functions
	private function Virus() {
		trace("Virus Created, instance: "+this);
	}
	private function init(){
		_x=130;
		_y=-100;
		is_dropping = true;
		hits = 120+(30*_parent.skill);
		total_hits = hits;
		is_prone = true;
		explosion_str = "mega";
		is_boss = true;
		is_kill_event = true;
		points = 1000;
		timer = 0;
	}
	function onEnterFrame() {
		_parent.updateBar(hits, total_hits);
		if (is_dropping) {
			_y += 3;
			if (_y>=160) {
				is_dropping = false;
			}
			_x = 300
		} else {
			var tmpHits = this.hits;
			if (tmpHits<10) {
				tmpHits = 10;
			}
			var speed = 1/tmpHits;
			timer += speed;
			var dx = 100+(200+(Math.sin(this.timer)*200));
			this._x = dx;
		}
		target_mc._x = _x;
		target_mc._y = _y;
		if (random(Math.round(500/_parent.skill)) == 1) {
			_parent.addAce(true, _x, _y);
		}
		if (random(Math.round(400/_parent.skill)) == 1) {
			_parent.fireEnemyRandom(this);
			_parent.fireEnemyRandom(this);
			_parent.fireEnemyRandom(this);
			_parent.fireEnemyRandom(this);
			_parent.fireEnemyRandom(this);
		}
		if (random(Math.round(600/_parent.skill)) == 1) {
			_parent.fireEnemyAimed(this)
		}
	}
}
