﻿import com.iainlobb.Proxy;
class com.robotboy.PointsPickUp extends MovieClip {
	// Symbols
	private var some_mc:MovieClip;
	// Variables
	private var xspeed:Number;
	private var yspeed:Number;
	private var timer:Number;
	private var points:Number;
	//
	private var display_score_str:String;
	// Functions
	private function PointsPickUp() {
		trace("PointsPickUp Created, instance: "+this);
	}
	function init(ini_x:Number, ini_y:Number, ini_points:Number){
		_x = ini_x;
		_y = ini_y
		xspeed = (0.5-Math.random())*4
		yspeed = (0.5-Math.random())*4
		timer = 0
		points = ini_points
	}
	function onEnterFrame() {
		if (_parent.player1_mc.weapon_array[6]>0) {
			var xdif:Number = this._x-_parent.player1_mc._x;
			var ydif:Number = this._y-_parent.player1_mc._y;
			var newXSpeed:Number = xspeed;
			var newYSpeed:Number = yspeed;
			newXSpeed -= xdif*(0.00035*_parent.player1_mc.weapon_array[6]);
			newYSpeed -= ydif*(0.00035*_parent.player1_mc.weapon_array[6]);
			newXSpeed = newXSpeed*0.98;
			newYSpeed = newYSpeed*0.98;
			this.xspeed = newXSpeed;
			this.yspeed = newYSpeed;
		}
		_x += xspeed;
		_y += yspeed;
		if (_parent.player1_mc.weapon_array[6]<=0) {
			if (_y<30) {
				_y = 30;
				yspeed = -yspeed;
			} else if (_y>_parent.bottom_limit) {
				_y = _parent.bottom_limit;
				yspeed = -yspeed;
			}
			if (_x<20) {
				_x = 20;
				xspeed = -xspeed;
			} else if (_x>_parent.right_limit-20) {
				_x = _parent.right_limit-20;
				xspeed = -xspeed;
			}
		}
		if (timer>20) {
			points = Math.max(points-1, 0);
		} else {
			timer++;
		}
		if (points == 0) {
			_alpha -= 3;
			if (this._alpha<0) {
				this.removeMovieClip();
			}
		} else if (_parent.player1_mc.hit_mc.hitTest(this._x, this._y, true)) {
			_parent.player1_mc.score += this.points;
			display_score_str = _parent.player1_mc.score+" k";
			_parent.sound_player_mc.startSound(6, "note", 60, 1);
			this.removeMovieClip();
		}
	}
}
