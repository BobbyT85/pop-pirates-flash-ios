﻿import com.iainlobb.Proxy;
class com.robotboy.WaveMaker extends MovieClip {
	// Symbols
	private var some_mc:MovieClip;
	// Variables
	private var timer:Number = 0;
	private var delay:Number = 10;
	private var total:Number;
	private var num:Number;
	private var skill:Number;
	private var the_x:Number;
	private var wobble:Number;
	//
	private var my_array:Array;
	// Functions
	private function WaveMaker() {
		trace("WaveMaker Created, instance: "+this);
	}
	private function onLoad(){
		//
	}
	public function init(ini_total:Number, ini_array:Array){
		total = ini_total;
		timer = 0;
		num = 0;
		my_array = ini_array;
		skill = _parent.skill*2
		the_x = random(_parent.right_limit-250)+60;
		wobble = random(10)+4;
	}
	function onEnterFrame() {
		timer++;
		if (timer>delay) {
			_parent.addEnemy({_x:the_x, _y:-150-(_parent.skill*10*skill), hits:1+(0.2*skill), theList:my_array, wobble:wobble});
			timer = 0;
			num++;
			if (num>=total) {
				delete this.onEnterFrame;
				this.removeMovieClip();
			}
		}
	}
}
