﻿import com.iainlobb.Proxy;
import com.iainlobb.Tools;
class com.robotboy.HeatSeeker extends com.robotboy.Weapon {
	// Symbols
	private var target_mc:MovieClip;
	private var owner_mc:MovieClip;
	// Variables
	private var is_target_found:Boolean;
	//
	private var speed:Number;
	private var strength:Number;
	// Functions
	private function HeatSeeker() {
		trace("HeatSeeker Created, instance: "+this);
	}
	function init(){
		owner_mc = _parent.player1_mc;
		_x = owner_mc._x
		_y = owner_mc._y
		speed = 8;
		strength = 2;
		is_target_found = false;
	}
	function onEnterFrame() {
		//
		if (!is_target_found) {
			_y -= speed;
			_rotation = 0;
		} else {
			if (typeof (target_mc._x) != "undefined") {
				var myAngle = 90-Tools.getAngle(_x, _y, target_mc._x, target_mc._y);
				_rotation = myAngle;
				var theta:Number = Tools.convertDeg(_rotation, 'rad');
				var dx:Number = Math.sin(theta)*speed;
				var dy:Number = Math.cos(theta)*speed;
				_x += dx;
				_y -= dy;
			} else {
				is_target_found = false;
			}
		}
		//heat targets
		if (!is_target_found) {
			var shortest:Number = 999999999999;
			var total_bads:Number = _parent.bad_array.length;
			for (var i:Number = 0; i<total_bads; i++) {
				var enemy_mc:MovieClip = _parent.bad_array[i];
				if (typeof (enemy_mc._x) != "undefined") {
					var op:Number = _x-enemy_mc._x;
					var adj:Number = _y-enemy_mc._y;
					var hyp:Number = Math.sqrt((op*op)+(adj*adj));
					if (hyp<shortest) {
						if (enemy_mc.is_prone) {
							target_mc = enemy_mc;
							shortest = hyp;
							is_target_found = true;
						}
					}
				}
			}
		}
		//intersects                                                                                                                                                                                                                                                                                                                                                                                                                       
		total_bads = _parent.bad_array.length;
		for (var i:Number = 0; i<total_bads; i++) {
			var enemy_mc:MovieClip = _parent.bad_array[i];
			if (enemy_mc.hitTest(_x, _y, true)) {
				if (typeof (enemy_mc.hitArea) == "movieclip") {
					if (enemy_mc.hitArea.hitTest(_x, _y, true)) {
						enemy_mc.hit(strength);
						delete this.onEnterFrame;
						removeMovieClip(this);
					}
				} else {
					enemy_mc.hit(strength);
					delete this.onEnterFrame;
					removeMovieClip(this);
				}
			}
		}
		testEdges();
	}
}
