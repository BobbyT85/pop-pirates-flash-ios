﻿import com.iainlobb.Proxy;
import com.iainlobb.Tools;
class com.robotboy.Game extends MovieClip {
	// Symbols
	private var gui_mc:MovieClip;
	private var black_mc:MovieClip;
	private var player1_mc:MovieClip;
	private var controller_mc:MovieClip;
	private var shadow_mc:MovieClip;
	private var sound_player_mc:MovieClip;
	private var info_mc:MovieClip;
	// Variables
	private var virgin:Boolean = true;
	private var mouse_hidden:Boolean = false;
	//
	private var bottom_limit:Number = 400;
	private var top_limit:Number = 50;
	private var right_limit:Number = 600;
	private var num_bullets:Number = 0;
	private var num_bad_bullets:Number = 0;
	private var num_bads:Number = 0;
	private var num_lists:Number = 0;
	private var num_pick_ups:Number = 0;
	private var respawn_time:Number = 0;
	private var monster_queue:Number = 0;
	private var num_makers:Number = 0;
	private var orb_timer:Number = 0;
	private var lastboss:Number = 1;
	private var skill:Number;
	private var the_depth:Number = 3;
	private var the_event:Number = 0;
	private var controller_timer:Number = 0;
	//
	private var bad_array:Array = [];
	private var pick_ups_array:Array = [];
	private var active_players_array:Array = [];
	private var bullet_array:Array = [];
	private var bad_bullet_array:Array = [];
	//
	private var display_score_str:String = "0 k";
	// Functions
	private function Game() {
		trace("Game Created, instance: "+this);
	}
	function onLoad():Void {
		Mouse.hide();
		_quality = "LOW";
		if (virgin) {
			attachMovie("blackscreen", "blackScreen", 7777776, {_x:340, _y:236, _yscale:102});
			gui_mc = attachMovie("gui", "gui_mc", 7777777);
			virgin = false;
		}
		black_mc._visible = false;
		//INITIALISE CONSTANTS / VARIABLES
		display_score_str = "0 k";
		the_depth = 3;
		the_event = 0;
		bad_array = [];
		pick_ups_array = [];
		active_players_array = [];
		bullet_array = [];
		bad_bullet_array = [];
		num_bullets = 0;
		num_bad_bullets = 0;
		num_bads = 0;
		num_lists = 0;
		num_pick_ups = 0;
		respawn_time = 100;
		monster_queue = 0;
		num_makers = 0;
		orb_timer = 0;
		lastboss = 1;
		skill = 1;
		// INIT PLAYER
		player1_mc.removeMovieClip();
		player1_mc = attachMovie("ship1", "player1_mc", 666666, {_x:200, _y:600});
		// INIT GAME CONTROLLER
		this.createEmptyMovieClip("controller_mc", 13241324);
		controller_mc.onEnterFrame = Proxy.create(this, controllerLoop);
	}
	//GAME EVENT PROGRESSION
	function controllerLoop() {
		controller_timer++;
		orb_timer += 0.2;
		if (controller_timer>=respawn_time) {
			nextEvent();
			controller_timer = 0;
		}
	}
	function nextEvent() {
		skill += 0.05;
		monster_queue++;
		//ADD A BOSS?
		if (Math.floor(skill) != lastboss) {
			the_event = 5+random(2);
			lastboss = Math.floor(skill);
		} else {
			the_event = random(5);
		}
		if (the_event == 0) {
			respawn_time = 400;
			startMaker("hunter", 1, 100);
		} else if (the_event == 2) {
			respawn_time = 1000;
			createWorm(true);
		} else if (the_event == 1) {
			respawn_time = 400;
			startMaker("wave", skill*2, 10);
		} else if (the_event == 3) {
			respawn_time = 400;
			addSpider();
		} else if (the_event == 4) {
			respawn_time = 400;
			addAces();
		} else if (the_event == 5) {
			respawn_time = 8000;
			createRobot();
			dropBar();
		} else if (the_event == 6) {
			respawn_time = 8000;
			addVirus();
			dropBar();
		}
	}
	function changeMonsterQueue() {
		monster_queue--;
		if (monster_queue<1) {
			controller_mc.timer = 0;
			nextEvent();
		}
	}
	function getTheDepth():Number {
		the_depth++;
		return the_depth;
	}
	function createExplosion(x, y):Void {
		var depth:Number = getTheDepth();
		var explosion_mc:MovieClip = attachMovie("explosion", "explosion"+(1000000+depth)+"_mc", 1000000+depth, {_x:x, _y:y+30, _xscale:200, _yscale:200});
	}
	function fireBullet(offset_x, dx, dy, strength, frame) {
		num_bullets++;
		var new_bullet_mc:MovieClip = attachMovie("Bullet", "bullet"+num_bullets+"_mc", 10000+num_bullets);
		new_bullet_mc.init(offset_x, dx, dy, strength, frame);
		bullet_array.push(new_bullet_mc);
	}
	function fireHeatSeeker() {
		num_bullets++;
		var new_bullet_mc:MovieClip = attachMovie("HeatSeeker", "bullet"+num_bullets+"_mc", 10000+num_bullets);
		new_bullet_mc.init();
		bullet_array.push(new_bullet_mc);
	}
	//PICK UPS
	function addPickUp(x, y, power_type) {
		num_pick_ups++;
		var new_pick_up_mc:MovieClip = this.attachMovie("PickUp", "pic_up"+num_pick_ups+"_mc", 6000+num_pick_ups);
		new_pick_up_mc.init(x, y, power_type);
		pick_ups_array.push(new_pick_up_mc);
	}
	function addMusicFile(x, y, points) {
		num_pick_ups++;
		var new_pick_up_mc:MovieClip = this.attachMovie("PointsPickUp", "music_file"+num_pick_ups, 6000+num_pick_ups);
		new_pick_up_mc.init(x, y, points);
		pick_ups_array.push(new_pick_up_mc);
	}
	function takeOrb() {
		this["orb"+player1_mc.weapon_array[5]].removeMovieClip();
	}
	function addOrb() {
		var orb_mc:MovieClip = this.attachMovie("orb", "orb"+player1_mc.weapon_array[5], player1_mc.weapon_array[5]);
	}
	function fireEnemyAimed(source_mc:MovieClip) {
		sound_player_mc.startBadSound(5, "enemy_gun", 60, 1);
		num_bad_bullets++;
		var new_bullet_mc:MovieClip = attachMovie("BadBullet", "bad_bullet"+num_bad_bullets+"_mc", 11000+num_bad_bullets);
		new_bullet_mc.init(source_mc._x, source_mc._y);
	}
	function fireEnemyRandom(source_mc:MovieClip) {
		sound_player_mc.startBadSound(5, "enemy_gun", 60, 1);
		num_bad_bullets++;
		var new_bullet_mc:MovieClip = attachMovie("RandomBadBullet", "bad_bullet"+num_bad_bullets+"_mc", 11000+num_bad_bullets);
		new_bullet_mc.init(source_mc._x, source_mc._y);
	}
	// DAMAGE AN ENEMY
	function createEnemyExplosion(x:Number, y:Number, scale:Number) {
		attachMovie("mega", "explosion"+(1000+num_bullets)+"_mc", 1000+num_bullets, {_x:x, _y:y, _xscale:100*scale, _yscale:100*scale});
	}
	// ADD ENEMIES - 57 VARIETIES!!!
	// WAVE ENEMY SHIPS
	function addEnemy(initProps) {
		num_bads++;
		var new_enemy_mc:MovieClip = attachMovie("enemy", "enemy"+num_bads, 110000+num_bads, {_x:initProps._x, _y:initProps._y});
		new_enemy_mc.init(initProps.hits, [], initProps.wobble);
		bad_array.push(new_enemy_mc);
		new_enemy_mc.myList.unshift(new_enemy_mc);
	}
	// PLAYER-TRACKING ENEMY SHIPS
	function addHunter(initProps) {
		num_bads++;
		var new_enemy_mc = attachMovie("hunter", "enemy"+num_bads, 110000+num_bads, {_x:initProps._x, _y:initProps._y});
		new_enemy_mc.init(initProps.hits, active_players_array[random(active_players_array.length)]);
		bad_array.push(new_enemy_mc);
		return new_enemy_mc;
	}
	// ENEMY SHIP MAKER - HUNTERS AND WAVES
	function startMaker(theType, theTotal, delay) {
		if (theType == "wave") {
			num_lists++;
			this["waveList"+num_lists] = [];
			num_makers++;
			var newBadMaker:MovieClip = attachMovie("WaveMaker", "bad_maker"+num_makers+"_mc", 99999+num_makers);
			newBadMaker.init(theTotal, this["waveList"+num_lists]);
		}
		if (theType == "hunter") {
			num_makers++;
			var newBadMaker:MovieClip = attachMovie("HunterMaker", "bad_maker"+num_makers+"_mc", 99999+num_makers);
			newBadMaker.init(theTotal, delay);
		}
	}
	// ROBOTIC SPIDERS
	function addSpider() {
		num_bads++;
		var newEn = attachMovie("spider", "spider"+num_bads, 110000+num_bads);
		bad_array.push(newEn);
		return newEn;
	}
	// GIANT WASP
	function addVirus():Void {
		num_bads++;
		var new_enemy_mc:MovieClip = attachMovie("Virus", "virus"+num_bads+"_mc", 110000+num_bads);
		new_enemy_mc.init();
		bad_array.push(new_enemy_mc);
	}
	// ACES - FAST, FREE-MOVING ENEMY SHIPS
	function addAce(virus, thex, they) {
		num_bads++;
		var enemy_mc:MovieClip = attachMovie("ace", "ace"+num_bads+"_mc", 110000+num_bads);
		bad_array.push(enemy_mc);
		return enemy_mc;
	}
	// ADD HOWEVER MANY ACES
	function addAces() {
		num_lists++;
		this["aceList"+num_lists] = [];
		for (var i = 0; i<3; i++) {
			var newAce = addAce();
			this["aceList"+num_lists].push(newAce);
			newAce.my_array = this["aceList"+num_lists];
		}
	}
	function createWorm() {
		num_lists++;
		this["wormList"+num_lists] = [];
		var num_segments:Number = Math.floor(5*skill);
		for (var i:Number = 0; i<=num_segments; i++) {
			num_bads++;
			var new_enemy_mc:MovieClip = attachMovie("worm", "enemy"+num_bads, 110000+num_bads);
			if (i == num_segments) {
				new_enemy_mc.init(true);
			} else {
				new_enemy_mc.init(false);
			}
			bad_array.push(new_enemy_mc);
		}
	}
	//ROBOTS (FORMERLY BOSSES, HENCE NAME)
	function createRobot() {
		num_lists++;
		this["bossList"+num_lists] = [];
		var head_mc:MovieClip = addRobotPart(1, null, 10, false, 100);
		var body_mc:MovieClip = addRobotPart(2, head_mc, 7, false, 100);
		var arm1_mc:MovieClip = addRobotPart(3, body_mc.arm1, 8, true, 100);
		var arm2_mc:MovieClip = addRobotPart(3, body_mc.arm2, 9, false, 100);
		var hand1_mc:MovieClip = addRobotPart(4, arm1_mc.hand, 11, true, 100);
		var hand2_mc:MovieClip = addRobotPart(4, arm2_mc.hand, 12, false, -100);
		var leg1_mc:MovieClip = addRobotPart(5, body_mc.leg1, 4, true, 100);
		var leg2_mc:MovieClip = addRobotPart(5, body_mc.leg2, 5, false, 100);
		var foot1_mc:MovieClip = addRobotPart(6, leg1_mc.foot, 1, false, 100);
		var foot2_mc:MovieClip = addRobotPart(6, leg2_mc.foot, 2, false, -100);
	}
	// ADD INDIVIDUAL PART
	function addRobotPart(the_type:Number, owner_mc:MovieClip, base:Number, flip:Boolean, xscale:Number) {
		num_bads++;
		var new_robot_mc:MovieClip = attachMovie("Robot", "robot"+num_bads+"_mc", 130000+(num_lists*20)+base);
		new_robot_mc.init(owner_mc, the_type, flip, xscale);
		bad_array.push(new_robot_mc);
		return new_robot_mc;
	}
	function killPlayer() {
		// DARKEN SCREEN:
		black_mc._visible = true;
		black_mc._alpha = 0;
		black_mc.onEnterFrame = function() {
			if (this._alpha<50) {
				this._alpha += 2;
			} else {
				delete this.onEnterFrame;
			}
		};
		//
		hideBar();
		Mouse.show();
		sound_player_mc.mixTo("menu");
		mouse_hidden = false;
		if (player1_mc.score != 0) {
			//add highscore
		}
		Tools.removeFromArray(active_players_array, player1_mc);
		display_score_str = "";
		Mouse.show();
		Mouse.removeListener(_global.mouse_listener);
		clearGame();
		info_mc._visible = true;
		info_mc.gotoAndPlay("gameover");
	}
	function clearGame() {
		var bad_array_length:Number = bad_array.length;
		for (var i:Number = 0; i<bad_array_length; i++) {
			bad_array[i].removeMovieClip();
		}
		var pic_strksListLength = pick_ups_array.length;
		for (i=0; i<pic_strksListLength; i++) {
			pick_ups_array[i].removeMovieClip();
		}
		var bulListLength = bullet_array.length;
		for (i=0; i<bulListLength; i++) {
			bullet_array[i].removeMovieClip();
		}
		var badBulletLength = bad_bullet_array.length;
		for (i=0; i<badBulletLength; i++) {
			bad_bullet_array[i].removeMovieClip();
		}
		delete controller_mc.onEnterFrame;
		controller_mc.removeMovieClip();
		for (var i:Number = 0; i<=num_makers; i++) {
			this["badMaker"+num_makers].removeMovieClip();
			this["spiderMaker"+num_makers].removeMovieClip();
		}
	}
	function killBullet(bullet_mc:MovieClip) {
		Tools.removeFromArray(bad_bullet_array, bullet_mc);
		bullet_mc.removeMovieClip();
	}
	//
	// GUI FUNCTIONS:
	function dropBar() {
		gui_mc.dropBar();
	}
	function hideBar() {
		gui_mc.hideBar();
	}
	function updateBar(hits:Number, total:Number) {
		gui_mc.updateBar(hits, total);
	}
	function showPickUp(num:Number) {
		gui_mc.showPickUp(num)
	}
}
