﻿import com.iainlobb.Proxy;
class com.robotboy.Bullet extends com.robotboy.Weapon {
	// Symbols
	private var owner_mc:MovieClip;
	// Variables
	private var dx:Number;
	private var dy:Number;
	private var strength:Number;
	// Functions
	private function Bullet() {
		trace("Bullet Created, instance: "+this);
	}
	function init(offset_x:Number, ini_dx:Number, ini_dy:Number, ini_strength:Number, ini_frame:Number){
		owner_mc = _parent.player1_mc
		_x = owner_mc._x+offset_x
		_y = owner_mc._y
		dx = ini_dx
		dy = ini_dy
		strength = ini_strength
		gotoAndStop(ini_frame);
	}
	function onEnterFrame() {
		testEdges();
		_y -= dy;
		_x += dx;
		var total_bads:Number = _parent.bad_array.length;
		for (var i:Number = 0; i<total_bads; i++) {
			var enemy_mc = _parent.bad_array[i];
			if (enemy_mc.hitTest(this._x, this._y, true)) {
				if (typeof (enemy_mc.hitArea) == "movieclip") {
					if (enemy_mc.hitArea.hitTest(this._x, this._y, true)) {
						enemy_mc.hit(strength)
						delete this.onEnterFrame;
						removeMovieClip(this);
					}
				} else {
					enemy_mc.hit(strength)
					delete this.onEnterFrame;
					removeMovieClip(this);
				}
			}
		}
	}
}
