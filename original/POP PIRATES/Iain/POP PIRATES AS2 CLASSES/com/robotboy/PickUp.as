﻿import com.iainlobb.Proxy;
class com.robotboy.PickUp extends MovieClip {
	// Symbols
	private var some_mc:MovieClip;
	// Variables
	private var timer:Number = 0;
	private var xspeed:Number;
	private var yspeed:Number;
	private var power_type:Number;
	// Functions
	private function PickUp() {
		trace("PickUp Created, instance: "+this);
	}
	public function init(ini_x:Number, ini_y:Number, ini_power_type:Number){
		_x = ini_x;
		_y = ini_y;
		xspeed = 1-(2*Math.random())
		yspeed = 1-(2*Math.random())
		power_type = ini_power_type
		//
		if (power_type == undefined) {
			power_type = random(6)+2;
		}
		if (power_type == "music") {
			power_type = 1;
		}
		gotoAndStop(power_type);
	}
	private function onEnterFrame() {
		timer++
		if (timer>600) {
			_alpha -= 2;
			if (_alpha<0) {
				this.removeMovieClip();
			}
		}
		_x += xspeed;
		_y += yspeed;
		if (_y<30) {
			_y = 30;
			yspeed = -yspeed;
		} else if (_y>_parent.bottom_limit) {
			_y = _parent.bottom_limit;
			yspeed = -yspeed;
		}
		if (_x<20) {
			_x = 20;
			xspeed = -xspeed;
		} else if (_x>_parent.right_limit-20) {
			_x = _parent.right_limit-20;
			xspeed = -xspeed;
		}
		if (_parent.player1_mc.hit_mc.hitTest(_x, _y, true)) {
			_parent.player1_mc.levelUp(power_type);
			this.removeMovieClip();
		}
	}
}
