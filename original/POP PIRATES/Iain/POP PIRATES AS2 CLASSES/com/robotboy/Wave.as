﻿import com.iainlobb.Proxy;
class com.robotboy.Wave extends com.robotboy.Enemy {
	// Symbols
	private var target_mc:MovieClip;
	// Variables
	private var timer:Number = 0;
	private var dir:Number = 1;
	private var speed:Number;
	private var skill:Number;
	private var hits:Number;
	private var points:Number = 200;
	private var wobble:Number = 0;
	//
	private var explosion_str:String = "mega";
	//
	private var is_prone:Boolean = false;
	private var fired:Boolean = false;
	//
	private var my_array:Array;
	// Functions
	private function Wave() {
		trace("Wave Created, instance: "+this);
	}
	private function onLoad() {
		//
	}
	private function init(ini_hits:Number, ini_array:Array, ini_wobble:Number) {
		hits = ini_hits;
		speed = 1+Math.random();
		is_prone = false;
		dir = 1;
		my_array = ini_array;
		timer = 0;
		points = 100;
		wobble = ini_wobble;
	}
	function onEnterFrame() {
		// SINE WAVE MOVEMENT
		timer += 0.1;
		var dx = Math.sin(this.timer)*wobble;
		this._x += dx;
		this._y += 1.5*(_parent.skill)*dir;
		// SHOOT
		if (_y>0 & _y<_parent.bottom_limit) {
			is_prone = true;
			if (!fired) {
				if (random(90/_parent.skill) == 0) {
					_parent.fireEnemyAimed(this)
					fired = true;
				}
			}
		}
		if (this._y>_parent.bottom_limit+300) {
			dir = -1;
			_yscale = -100;
			fired = false;
		} else if (_y<-300) {
			_yscale = 100;
			dir = 1;
			fired = false;
		}
		if (_parent.player1_mc.hit_mc.hitTest(_x, _y, true)) {
			_parent.hit(_parent.player1_mc);
		}
	}
}
