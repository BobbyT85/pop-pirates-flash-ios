﻿import com.iainlobb.Proxy;
class com.robotboy.Spider extends com.robotboy.Enemy {
	// Symbols
	private var drop_mc:MovieClip;
	// Variables
	private var is_prone:Boolean;
	public var is_kill_event:Boolean;
	//
	private var shoot:Number;
	private var fire_rate:Number;
	private var speed:Number;
	private var hits:Number;
	private var points:Number;
	private var timer:Number = 0;
	private var skill:Number;
	private var tx:Number;
	private var ty:Number;
	//
	private var explosion_str:String;
	// Functions
	private function Spider() {
		trace("Spider Created, instance: "+this);
	}
	private function onLoad() {
		_x = random(_parent.right_limit-50)+25;
		_y = -100;
		hits = 8*_parent.skill;
		is_prone = true;
		explosion_str = "mega";
		points = 300;
		timer = 0;
		ty = 50;
		skill = Math.sqrt(_parent.skill);
	}
	function onEnterFrame() {
		timer++;
		var total_goods:Number = _parent.active_players_array.length;
		for (var i:Number = 0; i<total_goods; i++) {
			var target_mc:MovieClip = _parent.active_players_array[i];
			if (this.hitTest(target_mc._x, target_mc._y, true)) {
				_parent.hit(target_mc);
			}
		}
		if (timer == Math.floor(10/skill)) {
			drop_mc.gotoAndPlay("drop");
			_parent.sound_player_mc.startSound(12, "spider_attack", 100, 1);
		}
		if (timer>Math.floor(9/skill) && timer<Math.floor(50/skill)) {
			_y += 8*skill;
		}
		if (timer == Math.floor(50/skill)) {
			gotoAndStop(1);
			_parent.sound_player_mc.startSound(12, "spidersound", 100, 1);
			var target_mc:MovieClip = _parent.active_players_array[random(_parent.active_players_array.length)];
			tx = target_mc._x;
		}
		if (timer>Math.floor(50/skill)) {
			_x += (tx-_x)/(10/skill);
			_y += (ty-_y)/(10/skill);
		}
		if (timer>Math.floor(80/skill)) {
			gotoAndStop(2);
			timer = 0;
		}
		if (_y>_parent.bottom_limit) {
			ty = 50;
		}
	}
}
