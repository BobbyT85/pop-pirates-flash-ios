﻿import com.iainlobb.Proxy;
class com.robotboy.SpiderMaker extends MovieClip {
	// Symbols
	private var some_mc:MovieClip;
	// Variables
	private var timer:Number;
	private var delay:Number;
	private var total:Number;
	private var num:Number;
	// Functions
	private function SpiderMaker() {
		trace("SpiderMaker Created, instance: "+this);
	}
	private function onLoad() {
		total = _parent.theTotal;
		delay = 234;
		timer = 220;
		num = 0;
	}
	function onEnterFrame() {
		timer++;
		if (timer>delay) {
			this.timer = 0;
			this.num++;
			var newSpider = addSpider();
			if (num>0) {
				newSpider.killEvent = true;
				delete this.onEnterFrame;
				this.removeMovieClip();
			}
		}
	}
}
