﻿import com.iainlobb.Proxy;
class com.robotboy.Orb extends MovieClip {
	// Symbols
	private var some_mc:MovieClip;
	// Variables
	private var orb_num:Number;
	//private var timer:Number;
	private var offsety:Number
	private var strength:Number = 1;
	// Functions
	private function Orb() {
		trace("Orb Created, instance: "+this);
	}
	private function onLoad(){
		offsety = 0;
		orb_num = _parent.player1_mc.weapon_array[5];
	}
	function init(){
		
	}
	function onEnterFrame() {
		var offset:Number = ((2*Math.PI)/_parent.player1_mc.weapon_array[5])*(this.orb_num-1);
		var tx:Number = Math.cos(_parent.orb_timer+offset)*70;
		var ty:Number = Math.sin(_parent.orb_timer+offset)*70;
		_x = (tx+_parent.player1_mc._x);
		_y = (ty+_parent.player1_mc._y)+offsety;
		var total_bads:Number = _parent.bad_array.length;
		for (var i:Number = 0; i<total_bads; i++) {
			var enemy_mc:MovieClip = _parent.bad_array[i];
			if (enemy_mc.hitTest(_x, _y, true)) {
				enemy_mc.hit(strength)
			}
		}
		var bad_bullet_array_length:Number = _parent.bad_bullet_array.length;
		for (var i:Number = 0; i<bad_bullet_array_length; i++) {
			var bullet_mc:MovieClip = _parent.bad_bullet_array[i];
			if (bullet_mc.hitTest(_x, _y, true)) {
				_parent.killBullet(bullet_mc);
			}
		}
	}
}
