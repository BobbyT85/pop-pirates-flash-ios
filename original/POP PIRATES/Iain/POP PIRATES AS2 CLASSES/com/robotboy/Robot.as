﻿import com.iainlobb.Proxy;
import com.iainlobb.Tools;
class com.robotboy.Robot extends com.robotboy.Enemy {
	// Symbols
	private var gun_hit_mc:MovieClip;
	private var owner_mc:MovieClip;
	private var backup_owner_mc:MovieClip;
	private var beam_mc:MovieClip;
	// Variables
	private var my_array:Array;
	//
	private var hits:Number;
	private var my_type:Number;
	private var points:Number;
	private var shoot:Number;
	private var mover:Number;
	private var target_x:Number;
	private var target_y:Number;
	private var character:Number;
	private var total_hits:Number;
	private var group_hits:Number;
	private var my_pos:Number;
	private var rotate:Number;
	//
	private var is_boss:Boolean;
	private var is_prone:Boolean;
	private var is_virgin:Boolean;
	private var is_firing:Boolean;
	private var flip:Boolean;
	// Functions
	private function Robot() {
		trace("Robot Created, instance: "+this);
	}
	function init(ini_owner_mc:MovieClip, ini_my_type:Number, ini_flip:Boolean, ini_xscale) {
		_x = random(_parent.right_limit);
		_y = random(_parent.bottom_limit);
		_xscale = ini_xscale;
		hits = 3*_parent.skill;
		owner_mc = ini_owner_mc;
		my_type = ini_my_type;
		is_prone = true;
		points = 300;
		is_virgin = true;
		shoot = 0;
		mover = 0;
		is_firing = false;
		my_array = _parent["bossList"+_parent.num_lists];
		my_array.push(this);
		flip = ini_flip;
		gotoAndStop(my_type);
		if (my_type == 4) {
			hits = 15*_parent.skill;
			hitArea = gun_hit_mc;
			gun_hit_mc._visible = false;
		}
		if (my_type == 1 || my_type == 2) {
			hits = 5*_parent.skill;
			is_prone = false;
		}
		if (my_type == 1) {
			is_boss = true;
			hits = 10*_parent.skill;
		}
		target_x = random(200)+100;
		target_y = 50+random(100);
		character = random(5);
		backup_owner_mc = owner_mc._parent.owner_mc;
		beam_mc.gotoAndStop(1);
	}
	// setFiring - THIS FUNCTION IS CALLED FROM TIMELINE TO SYNC WITH ANIMATION:
	function setFiring(firing:Boolean):Void{
		is_firing = firing;
	}
	function onEnterFrame() {
		if (is_virgin) {
			total_hits = 0;
			for (var i:Number = 0; i<my_array.length; i++) {
				total_hits += my_array[i].hits;
			}
			is_virgin = false;
		}
		group_hits = 0;
		if (my_type == 1 || this.my_type == 2) {
			for (var i:Number = 0; i<my_array.length; i++) {
				group_hits += my_array[i].hits;
			}
			_parent.updateBar(group_hits, total_hits);
			if (my_array.length<=2) {
				is_prone = true;
				if (random(Math.round(150/_parent.skill)) == 1) {
					_parent.fireEnemyRandom(this);
					_parent.fireEnemyRandom(this);
					_parent.fireEnemyRandom(this);
					_parent.fireEnemyRandom(this);
					_parent.fireEnemyRandom(this);
				}
			}
		}
		for (var i:Number = 0; i<this.my_array.length; i++) {
			if (my_array[i] == this) {
				my_pos = i;
			}
		}
		if (my_pos != 0) {
			if (typeof (owner_mc._x) == "undefined") {
				owner_mc = backup_owner_mc;
			}
		}
		var boss_pos:Object = {x:owner_mc._x, y:owner_mc._y};
		owner_mc._parent.localToGlobal(boss_pos);
		var op:Number = _x-boss_pos.x;
		var adj:Number = _y-boss_pos.y;
		var hyp:Number = Math.sqrt((op*op)+(adj*adj));
		var dx:Number = (-op)/2;
		var dy:Number = (-adj)/2;
		if (my_pos == 0) {
			shoot++;
			if (shoot*Math.random()>240/_parent.skill) {
				_parent.fireEnemyAimed(this)
				shoot = 0;
			}
			mover++;
			if (mover>100) {
				var foundfire:Boolean = false;
				for (var i:Number = 0; i<my_array.length; i++) {
					if (my_array[i].is_firing) {
						foundfire = true;
					}
				}
				if (!foundfire) {
					mover = 0;
					target_x = _parent.player1_mc._x;
					target_y = 100+random(_parent.bottom_limit/5);
				}
			}
			dx = (target_x-_x)/3;
			dy = (target_y-_y)/3;
		}
		_x += dx;
		_y += dy;
		if (my_type == 3 || my_type == 5) {
			if (character == 0) {
				if (random(100) == 0) {
					rotate = 5-random(10);
				}
			} else if (character == 1) {
				if (random(10) == 0) {
					rotate = 10-random(20);
				}
			} else if (character == 2) {
				if (flip) {
					if (_rotation>=60) {
						rotate = -3;
					}
					if (_rotation<=0) {
						rotate = 3;
					}
				} else {
					if (_rotation<=-60) {
						rotate = 3;
					}
					if (_rotation>=0) {
						rotate = -3;
					}
				}
			} else if (character == 3) {
				if (flip) {
					if (_rotation>=180) {
						rotate = -5;
					}
					if (_rotation<=0) {
						rotate = 5;
					}
				} else {
					if (_rotation<=-180) {
						rotate = 5;
					}
					if (this._rotation>=0) {
						rotate = -5;
					}
				}
			} else {
				if (random(10) == 0) {
					rotate = 10-random(20);
				}
			}
			_rotation += rotate;
			if (flip) {
				if (_rotation>60) {
					if (rotate>0) {
						_rotation = 60;
					}
				}
				if (_rotation<0) {
					if (rotate<0) {
						_rotation = 0;
					}
				}
			} else {
				if (_rotation<-60) {
					if (rotate<0) {
						_rotation = -60;
					}
				}
				if (_rotation>0) {
					if (rotate>0) {
						_rotation = 0;
					}
				}
			}
		}
		if (my_type == 4) {
			if (!is_firing) {
				var my_angle:Number = 270-Tools.getAngle(_x, _y, _parent.player1_mc._x, _parent.player1_mc._y);
				_rotation = my_angle;
				if (Math.round((Math.random()*300)/_parent.skill) == 1) {
					beam_mc.gotoAndPlay(2);
				}
			} else {
				if (this.hitTest(_parent.player1_mc._x, _parent.player1_mc._y, true)) {
					_parent.hit(_parent.player1_mc);
				}
			}
		}
		if (_y>_parent.bottom_limit+50) {
			this.removeMovieClip();
			Tools.removeFromArray(_parent.bad_array, this);
		}
	}
}
