﻿import com.iainlobb.Proxy;
import com.iainlobb.Tools;
class com.robotboy.Enemy extends MovieClip {
	// Symbols
	private var flasher_mc:MovieClip;
	// Variables
	private var my_color:Color;
	//
	private var flash_timer:Number = 0;
	private var hits:Number;
	private var points:Number;
	//
	private var explosion_str:String;
	//
	private var is_kill_event:Boolean;
	private var is_prone:Boolean;
	private var is_boss:Boolean;
	//
	private var my_array:Array;
	// Functions
	private function Enemy() {
		trace("Enemy Created, instance: "+this);
	}
	function flasherLoop() {
		flash_timer++;
		if (flash_timer>3) {
			my_color.setTransform({ra:100, rb:0, ga:100, gb:0, ba:100, bb:0, aa:100, ab:0});
			delete my_color;
			flash_timer = 0;
			flasher_mc.removeMovieClip();
		}
	}
	function hit(strength:Number) {
		_parent.sound_player_mc.startSound(7, "thud", 40, 1);
		if (is_prone) {
			my_color = new Color(this);
			my_color.setRGB(0xffffff);
			var flasher_mc:MovieClip = this.createEmptyMovieClip("flasher_mc", 100, {timer:0});
			flasher_mc.onEnterFrame = Proxy.create(this, flasherLoop);
			hits -= strength;
		}
		if (hits <= 0) {
			var scale:Number = 1;
			if (explosion_str == "mega") {
				scale = 2;
			}
			_parent.addMusicFile(_x, _y, points);
			_parent.sound_player_mc.startSound(7, "hit", 70, 1);
			_parent.createEnemyExplosion(_x, _y, scale)
			removeFromList();
			Tools.removeFromArray(_parent.bad_array, this);
			if (is_kill_event) {
				if (is_boss) {
					_parent.hideBar();
					var num_pick_ups:Number = Math.floor(_parent.skill);
					_parent.addPickUp(_x, _y, 7);
				} else {
					var num_pick_ups = Math.round(Math.random()*(_parent.skill/2));
				}
				for (var i:Number = 0; i<num_pick_ups; i++) {
					_parent.addPickUp(_x, _y);
				}
				_parent.changeMonsterQueue();
			}
			this.removeMovieClip();
		}
	}
	private function removeFromList():Void {
		for (var i:Number = 0; i<my_array.length; i++) {
			if (my_array[i] == this) {
				var my_pos:Number = i;
			}
		}
		my_array.splice(my_pos, 1);
		for (var i:Number = 0; i<my_array.length; i++) {
			my_array[i].my_pos = i;
		}
		if (my_array.length == 1) {
			my_array[0].is_kill_event = true;
		}
	}
}
