﻿import com.iainlobb.Proxy;
class com.robotboy.GUI extends MovieClip {
	// Symbols
	private var type_mc:MovieClip;
	private var bar_mc:MovieClip;
	private var power_mc:MovieClip;
	// Variables
	private var power_display_str:String;
	private var power_str:String;
	//
	private var text_index:Number;
	private var power_timer:Number;
	//
	private var pick_up_text_array:Array = ["nothing", "Basic", "Fire-Wall", "Killer-Watt", "Mega-Bite", "Anti-Virus", "Magnatron", "1UP"];
	// Functions
	private function GUI() {
		trace("GUI Created, instance: "+this);
	}
	private function onLoad() {
		type_mc.gotoAndStop(2);
	}
	function dropBar() {
		bar_mc.onEnterFrame = Proxy.create(this, dropBarLoop);
	}
	function dropBarLoop() {
		if (bar_mc._y<46) {
			bar_mc._y++;
		} else {
			delete bar_mc.onEnterFrame;
		}
	}
	function hideBar() {
		bar_mc.onEnterFrame = Proxy.create(this, hideBarLoop);
	}
	function hideBarLoop() {
		if (bar_mc._y>-29) {
			bar_mc._y--;
		} else {
			delete bar_mc.onEnterFrame;
		}
	}
	function updateBar(hits:Number, total:Number) {
		bar_mc.mask_mc._xscale = hits/total*100;
	}
	function powerLoop() {
		if (text_index<=power_str.length-1) {
			_parent.sound_player_mc.startSound(16, "blip", 100, 1);
			power_display_str += power_str.charAt(text_index);
		}
		text_index++;
		power_timer++;
		if (power_timer>100) {
			power_mc._alpha -= 2;
			if (power_mc._alpha<=0) {
				delete power_mc.onEnterFrame;
			}
		}
	}
	function showPickUp(num:Number) {
		power_display_str = "";
		text_index = 0;
		power_str = pick_up_text_array[num]+" x"+_parent.player1_mc.weapon_array[num];
		power_mc.icon_mc.gotoAndStop(num);
		power_timer = 0;
		power_mc._alpha = 100;
		power_mc.onEnterFrame = Proxy.create(this, powerLoop);
	}
}
